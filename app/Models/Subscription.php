<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
  use SoftDeletes;

  /**
   * @var string
   */
  protected $table = "subscription";
  /**
   * @var array
   */
  protected $dates = ['deleted_at'];
}
