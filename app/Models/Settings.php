<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Settings extends Model
{
  use SoftDeletes;

  /**
   * @var string
   */
  protected $table = "settings";
  /**
   * @var array
   */
  protected $dates = ['deleted_at'];


  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name', 'email', 'logo' , 'meta_descriptions', 'address_1', 'address_2', 'mobile', 'phone', 'gender', 'title', 'about'
  ];
}
