<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
  //
  use SoftDeletes;

  /**
   * @var string
   */
  protected $table = "countries";
  /**
   * @var array
   */
  protected $dates = ['deleted_at'];


      /**
       * The attributes that are mass assignable.
       *
       * @var array
       */
      protected $fillable = [
          'name', 'email', 'message'
      ];
}
