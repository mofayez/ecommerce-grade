<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FaqsTranslation extends Model
{
  use SoftDeletes;

  /**
   * @var string
   */
  protected $table = "faqs_translation";
  /**
   * @var array
   */
  protected $dates = ['deleted_at'];

  /**
   * @var array
   */
  protected $fillable = ['answer', 'question','lang_id'];


  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function faqs()
  {
      return $this->belongsTo('App\Models\Faqs', 'faqs_id');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function language()
  {
      return $this->belongsTo('App\Models\Languages', 'lang_id');
  }

}
