<?php

namespace App\Basket;

use App\Exceptions\QuantityExceededException;
use App\Support\Storage\Contracts\StorageInterface;

class Basket
{
    /**
     * Instance of StorageInterface.
     *
     * @var StorageInterface
     */
    protected $storage;

    /**
     * Create a new Basket instance.
     *
     * @param StorageInterface $storage
     */
    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
        // $this->product = $product;
    }

    /**
     * Check if the basket has a certain product.
     *
     * @param  product_option_value_id
     */
    public function has($product_option_value_id)
    {
        return $this->storage->exists($product_option_value_id);
    }

    /**
     * Update the basket.
     *
     * @param $product_option_value_id
     * @param         $quantity
     *
     * @throws QuantityExceededException
     */
    public function update($product_option_value_id, $quantity)
    {
        // if (!$this->product->find($product->id)->hasStock($quantity)) {
        //     throw new QuantityExceededException;
        // }

        if ($quantity == 0) {
            $this->remove($product_option_value_id);

            return;
        }

        $this->storage->set($product_option_value_id, [
            'product_option_value_id' => (int)$product_option_value_id,
            'quantity' => (int)$quantity,
        ]);
    }

    /**
     * Add the product with its quantity to the basket.
     * The quantity will be updated if it exists.
     *
     * @param $product_option_value_id
     * @param Integer $quantity
     */
    public function add($product_option_value_id, $quantity)
    {
        if ($this->has($product_option_value_id)) {
            $quantity = $this->get($product_option_value_id)['quantity'] + $quantity;
        }

        $this->update($product_option_value_id, $quantity);
    }

    /**
     * Remove a Product from the storage.
     *
     * @param $product_option_value_id
     */
    public function remove($product_option_value_id)
    {
        $this->storage->remove($product_option_value_id);
    }

    /**
     * Get a product that is inside the basket.
     *
     * @param $product_option_value_id
     */
    public function get($product_option_value_id)
    {
        return $this->storage->get($product_option_value_id);
    }

    /**
     * Clear the basket.
     */
    public function clear()
    {
        return $this->storage->clear();
    }

    /**
     * Get all products inside the basket.
     */
    public function all()
    {
        // $ids = [];
        // $items = [];
        //
        // foreach ($this->storage->all() as $product) {
        //     $ids[] = $product['product_option_value_id'];
        // }
        //
        // $products = $this->product->find($ids);
        //
        // foreach ($products as $product) {
        //     $product->quantity = $this->get($product)['quantity'];
        //     $items[] = $product;
        // }

        // return $items;

        return $this->storage->all();
    }

    /**
     * Get the amount of products inside the basket.
     */
    public function itemCount()
    {
        return count($this->storage->all());
    }

    /**
     * Get the subtotal price of all products inside the basket.
     */
    public function subTotal()
    {
        $total = 0;
        foreach ($this->all() as $item) {
            if ($item->outOfStock()) {
                continue;
            }

            $total += ($item->price * $item->quantity);
        }

        return $total;
    }

    /**
     * Check if the items in the basket are still in stock.
     */
    public function refresh()
    {
        foreach ($this->all() as $item) {
            if (!$item->hasStock($item->quantity)) {
                $this->update($item, $item->stock);
            }
        }
    }
}
