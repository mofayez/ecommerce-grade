<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Settings;
// use Image;

class SettingsControllerApi extends Controller
{
  /**
  * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
  */
  public function getIndex()
  {
    $settings = Settings::all();


    // check if no Settings
    if(!$settings){
      return [
          'status' => false,
          'data' => null,
          'msg' => 'There are no Settings in db!'
      ];
    }

    // check if status is true
    return [
        'status' => true,
        'data' => [
          'settings' => $settings
        ],
        'msg' => 'Settings had been successfully displayed!'
    ];
  }

  /**
  * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
  */
  public function getCreateNewSettings()
  {
    // check if status is true
    return [
        'status' => true,
        'data' => null,
        'msg' => 'Settings had been successfully displayed!'
    ];
  }

  /**
   * @param Request $request
   * @return array
   */
  public function createNewSettings(Request $request)
  {
    // validation roles
    $validation_settings = [
        'name' => 'required',
        'email' => 'required',
        'phone' => 'required',
        'mobile' => 'required',
        'gender' => 'required',
        'address_1' => 'required',
        'address_2' => 'required',
        'title' => 'required',
        'about' => 'required',
        'meta_description' => 'required',
        // 'logo' => 'required',
    ];

    $validation = validator($request->all(), $validation_settings);

    // if validation failed, return false response
    if ($validation->fails()) {
      foreach ($validation->errors()->all() as $key => $value) {
        return [
            'status' => false,
            'data' => 'validation error',
            'msg' => $value
        ];
      }
    }

    // $logo = $request->logo;
    // $filename = time() . '.' .$logo->getCilentOriginalExtension();
    // Image::make($logo)->resize(300,300)->save( public_path('/storage/uploads/images/avatars' . $filename));

    $settings = Settings::forceCreate([
      'name' => $request->name,
      'email' => $request->email,
      'mobile' => $request->mobile,
      'phone' => $request->phone,
      'address_1' => $request->address_1,
      'address_2' => $request->address_2,
      'gender' => $request->gender,
      'about' => $request->about,
      'title' => $request->title,
      'meta_descriptions' => $request->meta_description,
      'logo' => 'logo.jpg',
    ]);

    //check if no settings
    if (!$settings) {
        return [
            'status' => false,
            'data' => null,
            'msg' => 'There is no settings with such id!',
        ];
    }

    // check save status
    return [
        'status' => true,
        'data' => null,
        'msg' => 'Settings has been created successfully!',
    ];

  }

  /**
  * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
  */
  public function getUpdateSettings($id)
  {
    $settings = Settings::find($id);

    // chcek if no $settings
    if(!$settings){
      return [
        'status' => false,
        'data' => null,
        'msg' => 'There ara no settings with such id!'
      ];
    }

    //check delete successfully
    return [
        'status' => true,
        'data' => [
          'settings' => $settings
        ],
        'msg' => 'Settings has been displayed successfully!',
    ];
  }


  /**
   * @param $id
   * @param Request $request
   * @return array
   */
  public function updateSettings(Request $request, $id)
  {
    // validation roles
    $validation_settings = [
        'name' => 'required',
        'email' => 'required',
        'phone' => 'required',
        'mobile' => 'required',
        'gender' => 'required',
        'address_1' => 'required',
        'address_2' => 'required',
        'title' => 'required',
        'about' => 'required',
        'meta_description' => 'required',
        // 'logo' => 'required',
    ];

    $validation = validator($request->all(), $validation_settings);

    // if validation failed, return false response
    if ($validation->fails()) {
      foreach ($validation->errors()->all() as $key => $value) {
        return [
            'status' => false,
            'data' => 'validation error',
            'msg' => $value
        ];
      }
    }

    // get settings by id
    $settings = Settings::find($id);

    // check if no settings
    if(!$settings)
    {
      return [
          'status' => false,
          'data' => null,
          'msg' => 'There is no settings with such id!'
      ];
    }

    $settings->update([
      'name' => $request->name,
      'email' => $request->email,
      'mobile' => $request->mobile,
      'phone' => $request->phone,
      'address_1' => $request->address_1,
      'address_2' => $request->address_2,
      'gender' => $request->gender,
      'about' => $request->about,
      'title' => $request->title,
      'meta_descriptions' => $request->meta_description,
      'logo' => 'logo.jpg',
    ]);

    //check delete successfully
    return [
        'status' => true,
        'data' => null,
        'msg' => 'Settings has been updated successfully!',
    ];
  }

  /**
   * @param $id
   * @return array
   */
  public function deleteSettings($id)
  {

    //find settings by id
    $settings = Settings::find($id);

    //check if no settings
    if (!$settings) {
        //check status
        return [
            'status' => false,
            'data' => null,
            'msg' => 'There is No settings with such id!',
        ];
    }

    //delete settings
    $settings->delete();

    //check delete successfully
    return [
        'status' => true,
        'data' => null,
        'msg' => 'Settings has been deleted successfully!',
    ];
  }
}
