<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Faqs;


class FAQsControllerApi extends Controller
{
  /**
  * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
  */
  public function getIndex()
  {
    $faqs = Faqs::all();

    if(!$faqs)
    {
      return [
          'status' => false,
          'data' => null,
          'msg' => 'There ara no Faqs with such id!'
      ];
    }

    // append translated faq to all faqs
    foreach ($faqs as $faq) {

        // get faq details
        $faq->faq_translated = $faq->translate();
    }

    return [
        'status' => true,
        'data' => [
          'faqs' => $faqs
        ],
        'msg' => 'Data had been successfully displayed!'
    ];
  }

  /**
  * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
  */
  public function getCreateNewFaqs()
  {
    return [
        'status' => true,
        'data' => null,
        'msg' => 'Data had been successfully displayed!'
    ];
  }


  /**
   * @param Request $request
   * @return array
   */
  public function createNewFaqs(Request $request)
  {
    // validation faqs
    $validation_faqs = [
        'question_en' => 'required',
        'answer_en' => 'required',
    ];

    $validation = validator($request->all(), $validation_faqs);

    // if validation failed, return false response
    if ($validation->fails()) {
      foreach ($validation->errors()->all() as $key => $value) {
        return [
            'status' => false,
            'data' => 'validation error',
            'msg' => $value
        ];
      }
    }

    // choose one language to be the default one, let's make EN is the default
    // store master option
    // store the menu in en
    $en_id = Language::where('lang_code', 'en')->first()->id;


    $faqs = Faqs::forceCreate([
      'active' => $request->status
    ]);


    $faqs_en = $faqs->faqsTrans()->create([
      'faq_id' => $faqs->id,
      'lang_id' => $en_id,
      'question' => $request->question_en,
      'answer' => $request->answer_en,
    ]);


    // store ar version
    // because it is not required, we check if there is ar in request, then save it, else {no problem, not required}
    $faqs_ar = null;

    if ($request->question_ar) {

        $ar_id = Language::where('lang_code', 'ar')->first()->id;

        $faqs_ar = $faqs->faqsTrans()->create([
          'faq_id' => $faqs->id,
          'lang_id' => $ar_id,
          'question' => $request->question_ar,
          'answer' => $request->answer_ar,
        ]);
    }

    // check saving success
    return [
    'status' => true,
    'data' => null,
    'msg' => 'Faqs had been created successfully!',
    ];
  }

  /**
  * @param $id
  * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
  */
  public function getUpdateFaqs($id)
  {
      //find faqs by id
      $faqs = Faqs::find($id);

      // check if no faqs in such id
      if(!$faqs)
      {
          return [
          'status' => false,
          'data' => null,
          'msg' => 'There is no faqs with such id!'
          ];
      }

      //get menu details
      $faqs->en = $faqs->translate('en');
      $faqs->ar = $faqs->translate('ar');

      // check if status is true
      return [
      'status' => true,
      'data' => [
        'faqs' => $faqs
      ],
      'msg' => 'Data had been successfully displayed!'
      ];
  }

  /**
  * @param $id
  * @param Request $request
  * @return array
  */
  public function updateFaqs(Request $request, $id)
  {
      // validation faqs
      $validation_faqs = [
      'question_en' => 'required',
      'answer_en' => 'required',
      ];

      $validation = validator($request->all(), $validation_faqs);

      // if validation failed, return false response
      if ($validation->fails()) {
        foreach ($validation->errors()->all() as $key => $value) {
          return [
              'status' => false,
              'data' => 'validation error',
              'msg' => $value
          ];
        }
      }

      $faqs = Faqs::find($id);

      if(!$faqs)
      {
          return [
          'status' => false,
          'data' => null,
          'msg' => 'There is no faqs in such id!'
          ];
      }

      $faqs->update([
        'active' => $request->status
      ]);

      //check save success
      if ($faqs) {

          $en_id = Language::where('lang_code', 'en')->first()->id;

          $faqs_en = $faqs->translate('en');
          $faqs_en->question = $request->question_en;
          $faqs_en->answer = $request->answer_en;

          // check save status
          if (!$faqs_en->save()) {
              return [
              'status' => false,
              'data' => null,
              'msg' => 'something went wrong while updating EN, please try again!'
              ];
          }

          $faqs_ar = $faqs->translate('ar');

          if ($faqs_ar) {

              $faqs_ar->question = $request->question_ar;
              $faqs_ar->answer = $request->answer_ar;

              $faqs_ar->save();

          } else {

              if ($request->faqs_ar) {

                  $ar_id = Language::where('lang_code', 'ar')->first()->id;
                  $faqs_ar = new FaqsTranslation;

                  $faqs_ar->lang_id = $ar_id;
                  $faqs_ar->faq_id = $faqs->id;
                  $faqs_ar->menu = $request->question_ar;
                  $faqs_ar->answer = $request->answer_ar;

                  $faqs_ar->save();
              }

          }

          // check saving success
          return [
          'status' => true,
          'data' => null,
          'msg' => "FAQs data was updated successfully!",
          ];
      }

    }


    /**
    * @param $id
    * @return array
    */
    public function deleteFaqs($id)
    {
        //find faqs by id
        $faqs = Faqs::find($id);

        // check if no faqs
        if (!$faqs) {
            return [
            'status' => false,
            'data' => null,
            'msg' => 'There is no faqs with this id!!'
            ];
        }

        //delete data from faqsTrans
        $faqs->faqsTrans()->delete();

        //delete data from faqs
        $faqs->delete();

        // check saving success
        return [
        'status' => true,
        'data' => null,
        'msg' => 'FAQs has been deleted successfully!',
        ];
    }

}
