<?php

namespace App\Http\Controllers\API;

use App\Models\Bought;
use App\Models\BoughtDetails;
use App\Models\Product;
use App\Models\ProductTranslation;
use App\Models\ProductOptionValues;
use App\Models\ProductOptionValuesDetails;
use App\Models\OptionValues;
use App\Models\OptionValuesTranslation;
use App\Models\OptionTranslation;
use App\Models\NormalProductDetails;
use App\Models\User;
use App\Models\Role;
use App\Models\Option;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class BoughtsController
 * @package App\Http\Controllers\API
 */
class BoughtsControllerApi extends Controller
{
  /**
    * getIndex function
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
  public function getIndex()
  {
    $boughts = Bought::all();

    foreach ($boughts as $key => $bought) {
      $bought_details = $bought->boughtDetails()->first();
      $bought->bought_details = $bought_details;
    }

    return [
      'status' => 'true',
      'data' => [
        'boughts' => $boughts
      ],
      'msg' => 'Data has been successfully displayed!'

    ];
  }

  /**
    * getCreateNewBought function
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
  public function getCreateNewBought(Request $request)
  {
      // get role_id  where role = vendor
      $role_id = Role::where('role','vendor')->first()->id;

      // get all users that wit role vendor
      $users = User::where('role_id', $role_id)->get();

      if(!$users){
        return [
          'status' => false,
          'data' => null,
          'msg' => 'There is no users for vendor`s role'
        ];
      }

      // get all products
      $products = Product::all();

      // append translated products
      foreach ($products as $product) {

          // get product details
          $product->trans = $product->translate();
      }

      // get all Options
      $options = Option::all();

      // append translated option
      foreach ($options as $option) {

          // get option details
          $option->trans = $option->translate();
          $option->values = $option->optionValues;

          foreach($option->values as $value) {
              $value->trans = $value->translate();
          }
      }

      // check if status is true
      return [
        'status' => true,
        'data' => [
          'users' => $users,
          'products' => $products,
          'options' => $options,
        ],
        'msg' => 'Boughts has been successfully displayed!',
      ];
  }

  /**
    * getBoughtSectionView function
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
  public function getBoughtSectionView()
  {
      // get all products
      $products = Product::all();

      // append translated products
      foreach ($products as $product) {

          // get product details
          $product->trans = $product->translate();
      }

      // get all Options
      $options = Option::all();

      // append translated option
      foreach ($options as $option) {

          // get option details
          $option->trans = $option->translate();
          $option->values = $option->optionValues;

          foreach($option->values as $value) {
              $value->trans = $value->translate();
          }

      }

      //check if status is true
      return [
        'status' => true,
        'data' => [
          'products' => $products,
          'options' => $options
        ],
        'msg' => 'Data has been successfully displayed!'
      ];
  }

  /**
    * getUpdateBoughtSectionView function
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
  public function getUpdateBoughtSectionView()
  {
      // get all products
      $products = Product::all();

      // append translated products
      foreach ($products as $product) {

          // get product details
          $product->trans = $product->translate();
      }

      // get all Options
      $options = Option::all();

      // append translated option
      foreach ($options as $option) {

          // get option details
          $option->trans = $option->translate();
          $option->values = $option->optionValues;

          foreach($option->values as $value) {
              $value->trans = $value->translate();
          }

      }

      // check if status is true
      return [
        'status' => true,
        'data' => [
          'products' => $products,
          'options' => $options
        ],
        'msg' => 'Data has been successfully displayed!'
      ];
  }

  /**
    * getOptionSectionView function
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
  public function getOptionSectionView()
  {
      // get all Options
      $options = Option::all();

      // append translated option
      foreach ($options as $option) {

          // get option details
          $option->trans = $option->translate();
          $option->values = $option->optionValues;

          foreach($option->values as $value) {
              $value->trans = $value->translate();
          }

      }

      //chech if status is true
      return [
        'status' => true,
        'data' => [
          'options' => $options
        ],
        'msg' => 'Data has been successfully displayed!'
      ];
  }

  /**
  * get option's values
  */
  public function getOptionValues($id) {

      // get get optionValues for this option id
      $values = Option::find($id)->optionValues;

      // check if no values
      if(!$values)
      {
        return [
          'status' => false,
          'data' => null,
          'msg' => 'There are  no Values!'
        ];
      }

      // append values translate into values
      foreach ($values as $value) {

          $value->trans = $value->translate();
      }

      // check if status is true
      return [
        'status' => true,
        'data' => [
          'values' => $values
        ],
        'msg' => 'Data has been successfully displayed!'
      ];
  }

  /**
  * get option's values
  */
  public function getUpdateOptionValues($id) {

      // get get optionValues for this option id
      $values = Option::find($id)->optionValues;

      // check if no values
      if(!$values)
      {
        return [
          'status' => false,
          'data' => null,
          'msg' => 'There are  no Values!'
        ];
      }

      // append values translate into values
      foreach ($values as $value) {

          $value->trans = $value->translate();
      }

      // check if status is true
      return [
        'status' => true,
        'data' => [
          'values' => $values
        ],
        'msg' => 'Data has been successfully displayed!'
      ];
  }


  /**
    * createNewBought function
    *  @param Request request
    *  @return array
    */
  public function createNewBought(Request $request)
  {
      $validation_boughts = [

      ];

      $validation = validator($request->all(), $validation_boughts);

      // if validation failed, return false response
      if ($validation->fails()) {
        foreach ($validation->errors()->all() as $key => $value) {
          return [
              'status' => false,
              'data' => 'validation error',
              'msg' => $value
          ];
        }
      }

      // request noram_products
      $normal_products = $request->normal_products;

      // request option_products
      $option_products = $request->option_products;

      // rquest mainBought
      $mainBought = $request->mainBought;

      // create new Bought
      $Bought = new Bought;

      // if boughts have normal and option products
      if ($normal_products && $option_products){

          // var noraml_amount with init value 0
          $normal_amount = 0;

          // var total_normal_price with init value 0
          $total_normal_price = 0;

          // var total_price with init value 0
          $total_price = 0;

          foreach ($normal_products as $key => $pro_normal) {

              // get product_id from normal product request
              $product_id = $normal_products[$key][0]['id'];

              // find product for this product_id
              $product = Product::find($product_id);

              // check if no product in such id
              if(!$product){
                  return [
                      'status' => false,
                      'data' => null,
                      'msg' => 'There is no product with such id!'
                  ];
              }

              // get normal_product_details
              $normal_product = NormalProductDetails::where('product_id', $product_id)->first();

              // if normal_product is found
              if($normal_product){

                // update normal product details
                $normal_product->update([
                  'discount_type' => $normal_products[$key][0]['discount_type'],
                  'price' => $normal_products[$key][0]['price'],
                  'serial' => $normal_products[$key][0]['serial'],
                  'model_number' => $normal_products[$key][0]['model_number'],
                  'barcode' => $normal_products[$key][0]['barcode'],
                  'discount' => $normal_products[$key][0]['discount'],
                  'stock' => $normal_products[$key][0]['stock'],
                ]);

                // add all amount in noraml_ amount var
                $normal_amount += $normal_products[$key][0]['amount'];

                // get all noraml prices
                $normal_price = $normal_products[$key][0]['amount'] * $normal_products[$key][0]['price'];

                // // add all prices in total_normal_price var
                $total_normal_price += $normal_price;

                continue;

              }else{

                // store data in normal product details table
                $normal_products_details = $product->normalProductDetails()->forceCreate([
                    'product_id' => $product_id,
                    'discount_type' => $normal_products[$key][0]['discount_type'],
                    'price' => $normal_products[$key][0]['price'],
                    'serial' => $normal_products[$key][0]['serial'],
                    'model_number' => $normal_products[$key][0]['model_number'],
                    'barcode' => $normal_products[$key][0]['barcode'],
                    'discount' => $normal_products[$key][0]['discount'],
                    'stock' => $normal_products[$key][0]['stock'],
                ]);

                // check if no normal_product_details
                if(!$normal_products_details){
                    return [
                        'status' => false,
                        'data' => null,
                        'msg' => 'There is no normal product deatils!'
                    ];
                }

                // add all amount in noraml_ amount var
                $normal_amount += $normal_products[$key][0]['amount'];

                // get all noraml prices
                $normal_price = $normal_products[$key][0]['amount'] * $normal_products[$key][0]['price'];

                // // add all prices in total_normal_price var
                $total_normal_price += $normal_price;

              }
          }

          // var option_amount with inint value 0
          $option_amount = 0;

          // var total_option_price with inint value 0
          $total_option_price = 0;

          foreach ($option_products as $option_key => $pro_option) {

                  // get options_array
                  $options_array = $option_products[$option_key][0]['options_array'];

                  // get product_id
                  $product_id =$option_products[$option_key][0]['id'];

                  // find product for this product_id
                  $product = Product::find($product_id);

                  // check if no product
                  if(!$product){
                      return [
                          'status' => false,
                          'data' => null,
                          'msg' => 'There is no product with such id!'
                      ];
                  }

                  $product_option_values = ProductOptionValues::where('product_id', $product_id)->first();

                  if($product_option_values){

                    // store data in product_option_value
                    $pro_opt_vals = $product->productOptionValue()->forceCreate([
                        'product_id' => $option_products[$option_key][0]['id'],
                        'price' => $option_products[$option_key][0]['price'],
                        'serial' => $option_products[$option_key][0]['serial'],
                        'model_number' => $option_products[$option_key][0]['model_number'],
                        'barcode' => $option_products[$option_key][0]['barcode'],
                        'discount' => $option_products[$option_key][0]['discount'],
                        'stock' => $option_products[$option_key][0]['stock'],
                    ]);

                    // get product_option_value_id
                    $product_option_value_id = $product_option_values->id;

                    // get option values ids (2d array)
                    $opt_val_ids = ProductOptionValuesDetails::where('product_option_value_id', $product_option_value_id)->get(['option_value_id'])->toArray();

                    // covert 2d array to 1d array
                    $product_option_values_details_ids = array_map('current', $opt_val_ids);

                    // check if no $product_option_values_details_ids
                    if(!$product_option_values_details_ids){
                      return [
                        'status' => false,
                        'data' => null,
                        'msg' => 'There is no product option value details!'
                      ];
                    }

                    // get option values
                    $option_values = $options_array[0]['option_values'];

                    if(array_diff($product_option_values_details_ids, $option_values) == null){

                      // get all option amount
                      $option_amount += $option_products[$option_key][0]['amount'];

                      // get all option prices
                      $option_price = $option_products[$option_key][0]['amount'] * $option_products[$option_key][0]['price'];

                      // store all option prices in total_price
                      $total_option_price += $option_price;

                      // store total_amount for normal and option products
                      $total_amount = $normal_amount + $option_amount;

                      // store total_price for normal and option products
                      $total_price = $total_normal_price + $total_option_price;

                      continue;

                    }else{

                      foreach ($option_values as $opt_key => $opt_array) {
                        $product_option_values->productOptionValueDetails()->forceCreate([
                          'option_value_id' => $opt_array,
                          'product_option_value_id' => $pro_opt_vals->id
                        ]);
                      }
                    }

                    // get all option amount
                    $option_amount += $option_products[$option_key][0]['amount'];

                    // get all option prices
                    $option_price = $option_products[$option_key][0]['amount'] * $option_products[$option_key][0]['price'];

                    // store all option prices in total_price
                    $total_option_price += $option_price;

                    // store total_amount for normal and option products
                    $total_amount = $normal_amount + $option_amount;

                    // store total_price for normal and option products
                    $total_price = $total_normal_price + $total_option_price;

                    continue;

                  }else{

                    // store data in product_option_value
                    $product_option_value = $product->productOptionValue()->forceCreate([
                        'product_id' => $product_id,
                        'price' => $option_products[$option_key][0]['price'],
                        'serial' => $option_products[$option_key][0]['serial'],
                        'model_number' => $option_products[$option_key][0]['model_number'],
                        'barcode' => $option_products[$option_key][0]['barcode'],
                        'discount' => $option_products[$option_key][0]['discount'],
                        'stock' => $option_products[$option_key][0]['stock'],
                    ]);

                    // check if no product_option_value
                    if(!$product_option_value){
                        return [
                            'status' => false,
                            'data' => null,
                            'msg' => 'There is somthing wrong please try again!!'
                        ];
                    }

                    // get product_option_value_id
                    $product_option_value_id = $product_option_value->id;

                    // find product_option_values with such id
                    $product_opt_values = ProductOptionValues::find($product_option_value_id);

                    // check if no product_option_values
                    if(!$product_opt_values){
                        return [
                            'status' => false,
                            'data' => null,
                            'msg' => 'There is no product option values with such id!'
                        ];
                    }

                    // get options_array
                    $options_array = $option_products[$option_key][0]['options_array'];

                    foreach ($options_array[0]['option_values'] as $opt_key => $opt_array) {
                            $product_opt_values->productOptionValueDetails()->forceCreate([
                              'option_value_id' => $opt_array,
                              'product_option_value_id' => $product_option_value_id
                            ]);
                    }

                    // get all option amount
                    $option_amount += $option_products[$option_key][0]['amount'];

                    // get all option prices
                    $option_price = $option_products[$option_key][0]['amount'] * $option_products[$option_key][0]['price'];

                    // store all option prices in total_price
                    $total_option_price += $option_price;

                    // store total_amount for normal and option products
                    $total_amount = $normal_amount + $option_amount;

                    // store total_price for normal and option products
                    $total_price = $total_normal_price + $total_option_price;

                  }

                }

          // store total_amount
          $Bought->total_amount = $total_amount;

          // store total_price
          $Bought->total_price = $total_price;

          // get bought_type
          $bought_type = $mainBought[0]['bought_type'];

          // check if bought_type == cache
          if($bought_type == 'cache'){

              // store total_price in paid
              $Bought->paid = $total_price;

              // store 0 in remain
              $Bought->remain = '0';

          }else if ($bought_type == 'remainder'){

            // get data in paid
            $bought_paid = $mainBought[0]['paid'];

            // store bought_paid in Bought model
            $Bought->paid = $bought_paid;

            // get bought remain
            $bought_remain = $total_price - $bought_paid;

            // store remain in Bought model
            $Bought->remain = $bought_remain;

          }

          // get user_id
          $user_id = $mainBought[0]['user_id'];

          // store user_id in Bought model
          $Bought->user_id = $user_id;

          // store bought_type in Bought model
          $Bought->type = $bought_type;

          // if Bought save
          if($Bought->save()){

            foreach ($normal_products as $key => $pro_normal) {

              // store Bough_details for noraml products
              $Bought_Details = $Bought->boughtDetails()->forceCreate([
                'bought_id' => $Bought->id,
                'amount' => $normal_products[$key][0]['amount'],
                'cost' => $normal_products[$key][0]['cost'],
                'product_type' => $normal_products[$key][0]['type'],
                'product_id' => $normal_products[$key][0]['id'],
                'product_option_value_id' => null,
              ]);
            }


            foreach ($option_products as $option_key => $pro_option) {

              // get product_id
              $pro_id = $option_products[$option_key][0]['id'];

              // get product option value id with this id
              $pro_option_val_id = ProductOptionValues::where('product_id', $pro_id)->first()->id;

              // store Bough_details for option products
              $Bought_Details = $Bought->boughtDetails()->forceCreate([
                'bought_id' => $Bought->id,
                'amount' => $option_products[$option_key][0]['amount'],
                'cost' => $option_products[$option_key][0]['cost'],
                'product_type' => $option_products[$option_key][0]['type'],
                'product_id' => $option_products[$option_key][0]['id'],
                'product_option_value_id' => $pro_option_val_id,
              ]);
            }
          }
      }
      else if($option_products){

          // var $bought_id with init value null
          $bought_id = null;

          foreach ($option_products as $option_key => $pro_option) {

              // get options array
              $options_array = $option_products[$option_key][0]['options_array'];

              // get product_id
              $product_id =$option_products[$option_key][0]['id'];

              // find product for this product_id
              $product = Product::find($product_id);

              // check if no product
              if(!$product){
                  return [
                      'status' => false,
                      'data' => null,
                      'msg' => 'There is no product with such id!'
                  ];
              }

              // get product option Values by product_id
              $product_option_values = ProductOptionValues::where('product_id', $product_id)->first();

              // check if found
              if($product_option_values){

                // get product_option_value_id
                $product_option_value_id = $product_option_values->id;

                // get option values ids (2d array)
                $opt_val_ids = ProductOptionValuesDetails::where('product_option_value_id', $product_option_value_id)->get(['option_value_id'])->toArray();

                // covert 2d array to 1d array
                $product_option_values_details_ids = array_map('current', $opt_val_ids);

                // check if no $product_option_values_details_ids
                if(!$product_option_values_details_ids){
                  return [
                    'status' => false,
                    'data' => null,
                    'msg' => 'There is no product option value details!'
                  ];
                }

                // get option values
                $option_values = $options_array[0]['option_values'];

                // check if two array equals
                if(array_diff($product_option_values_details_ids, $option_values) == null){

                  $stock = $product_option_values->stock;
                  // get stock
                  $amount = $option_products[$option_key][0]['amount'];

                  $stock = $stock + $amount;

                  // store data in product_option_value
                  $pro_opt_vals = $product->productOptionValue()->update([
                    'product_id' => $option_products[$option_key][0]['id'],
                    'price' => $option_products[$option_key][0]['price'],
                    'serial' => $option_products[$option_key][0]['serial'],
                    'model_number' => $option_products[$option_key][0]['model_number'],
                    'barcode' => $option_products[$option_key][0]['barcode'],
                    'discount' => $option_products[$option_key][0]['discount'],
                    'stock' => $stock,
                  ]);

                  $cost = $option_products[$option_key][0]['cost'];
                  $product_type = $option_products[$option_key][0]['type'];
                  $product_id = $option_products[$option_key][0]['id'];
                  $bought_type = $mainBought[0]['bought_type'];
                  $user_id = $mainBought[0]['user_id'];

                  // check if bought_id = null
                  if($bought_id == null){

                    $amount1 = $option_products[$option_key][0]['amount'];
                    $cost1 = $option_products[$option_key][0]['cost'];
                    $totalPrice1 = $amount1 * $cost1;

                    // store data in bought
                    $bought = $Bought->forceCreate([
                      'user_id' => $user_id,
                      'total_amount' => $amount1,
                      'total_price' => $totalPrice1,
                      'type' => $bought_type,
                      'paid' => $totalPrice1,
                      'remain' => '0',
                    ]);

                    // check if bought_type == cache
                    if($bought_type == 'cache'){

                      $bought->paid = $totalPrice1;
                      $bought->remain = 0;

                    }elseif ($bought_type == 'remainder') {

                      $paid = $mainBought[0]['paid'];
                      $bought->paid = $paid;

                      $remain = $totalPrice1 - $paid;
                      $bought->remain = $remain;

                    }

                    // save changes
                    $bought->save();

                    // get $bought_id
                    $bought_id = $bought->id;

                    // store data in bought_details
                    $bought_details = $bought->boughtDetails()->forceCreate([
                        'bought_id' => $bought_id,
                        'amount' => $amount1,
                        'cost' => $cost,
                        'product_type' => $product_type,
                        'product_id' => $product_id,
                        'product_option_value_id' => $product_option_value_id,
                    ]);

                  }else{

                    $amount = $option_products[$option_key][0]['amount'];
                    $cost = $option_products[$option_key][0]['cost'];
                    $totalPrice = $amount * $cost;

                    $total_amount = $amount + $amount1;
                    $total_price = $totalPrice + $totalPrice1;

                    // find bought by id
                    $bought = Bought::find($bought_id);

                    // uodate data to bought
                    $bought->user_id = $user_id;
                    $bought->total_amount = $total_amount;
                    $bought->total_price = $total_price;
                    $bought->type = $bought_type;

                    // check if $bought_type == cache
                    if($bought_type == 'cache'){

                      $bought->paid = $total_price;
                      $bought->remain = 0;

                    }elseif ($bought_type == 'remainder') {

                      $paid = $mainBought[0]['paid'];
                      $bought->paid = $paid;

                      $remain = $total_price - $paid;
                      $bought->remain = $remain;

                    }

                    // save updated data
                    $bought->save();

                    // store data at bought_details
                    $bought_details = $bought->boughtDetails()->forceCreate([
                        'bought_id' => $bought_id,
                        'amount' => $amount,
                        'cost' => $cost,
                        'product_type' => $product_type,
                        'product_id' => $product_id,
                        'product_option_value_id' => $product_option_value_id,
                    ]);

                  }

                  continue;

                }else{

                  // store data in product_option_value
                  $pro_opt_vals = $product->productOptionValue()->forceCreate([
                      'product_id' => $option_products[$option_key][0]['id'],
                      'price' => $option_products[$option_key][0]['price'],
                      'serial' => $option_products[$option_key][0]['serial'],
                      'model_number' => $option_products[$option_key][0]['model_number'],
                      'barcode' => $option_products[$option_key][0]['barcode'],
                      'discount' => $option_products[$option_key][0]['discount'],
                      'stock' => $option_products[$option_key][0]['amount'],
                  ]);

                  // check if no $pro_opt_vals
                  if(!$pro_opt_vals){
                      return [
                          'status' => false,
                          'data' => null,
                          'msg' => 'There is no product option values!'
                      ];
                  }

                  // get product_option_value_id
                  $product_option_value_id = $pro_opt_vals->id;

                  // find product_option_values with such id
                  $product_opt_values = ProductOptionValues::find($product_option_value_id);

                  // check if no product_option_values
                  if(!$product_opt_values){
                      return [
                          'status' => false,
                          'data' => null,
                          'msg' => 'There is no product option values with such id!'
                      ];
                  }

                  // get options_array
                  $options_array = $option_products[$option_key][0]['options_array'];

                  foreach ($options_array[0]['option_values'] as $opt_key => $opt_array) {
                          $product_opt_values->productOptionValueDetails()->forceCreate([
                            'option_value_id' => $opt_array,
                            'product_option_value_id' => $product_option_value_id
                          ]);
                  }

                  $cost = $option_products[$option_key][0]['cost'];
                  $product_type = $option_products[$option_key][0]['type'];
                  $product_id = $option_products[$option_key][0]['id'];
                  $bought_type = $mainBought[0]['bought_type'];
                  $user_id = $mainBought[0]['user_id'];

                  // check if bought_id = null
                  if($bought_id == null){

                    $amount1 = $option_products[$option_key][0]['amount'];
                    $cost1 = $option_products[$option_key][0]['cost'];
                    $totalPrice1 = $amount1 * $cost1;

                    // store data in bought
                    $bought = $Bought->forceCreate([
                      'user_id' => $user_id,
                      'total_amount' => $amount1,
                      'total_price' => $totalPrice1,
                      'type' => $bought_type,
                      'paid' => $totalPrice1,
                      'remain' => '0',
                    ]);

                    // check if bought_type == cache
                    if($bought_type == 'cache'){

                      $bought->paid = $totalPrice1;
                      $bought->remain = 0;

                    }elseif ($bought_type == 'remainder') {

                      $paid = $mainBought[0]['paid'];
                      $bought->paid = $paid;

                      $remain = $totalPrice1 - $paid;
                      $bought->remain = $remain;

                    }

                    // save changes
                    $bought->save();

                    // get $bought_id
                    $bought_id = $bought->id;

                    // store data in bought_details
                    $bought_details = $bought->boughtDetails()->forceCreate([
                        'bought_id' => $bought_id,
                        'amount' => $amount1,
                        'cost' => $cost,
                        'product_type' => $product_type,
                        'product_id' => $product_id,
                        'product_option_value_id' => $product_option_value_id,
                    ]);

                  }else{

                    $amount = $option_products[$option_key][0]['amount'];
                    $cost = $option_products[$option_key][0]['cost'];
                    $totalPrice = $amount * $cost;

                    $total_amount = $amount + $amount1;
                    $total_price = $totalPrice + $totalPrice1;

                    // find bought by id
                    $bought = Bought::find($bought_id);

                    // uodate data to bought
                    $bought->user_id = $user_id;
                    $bought->total_amount = $total_amount;
                    $bought->total_price = $total_price;
                    $bought->type = $bought_type;

                    // check if $bought_type == cache
                    if($bought_type == 'cache'){

                      $bought->paid = $total_price;
                      $bought->remain = 0;

                    }elseif ($bought_type == 'remainder') {

                      $paid = $mainBought[0]['paid'];
                      $bought->paid = $paid;

                      $remain = $total_price - $paid;
                      $bought->remain = $remain;

                    }

                    // save updated data
                    $bought->save();

                    // store data at bought_details
                    $bought_details = $bought->boughtDetails()->forceCreate([
                        'bought_id' => $bought_id,
                        'amount' => $amount,
                        'cost' => $cost,
                        'product_type' => $product_type,
                        'product_id' => $product_id,
                        'product_option_value_id' => $product_option_value_id,
                    ]);

                  }

                }

                continue;

              }else{

                // store data in product_option_value
                $product_option_value = $product->productOptionValue()->forceCreate([
                    'product_id' => $product_id,
                    'price' => $option_products[$option_key][0]['price'],
                    'serial' => $option_products[$option_key][0]['serial'],
                    'model_number' => $option_products[$option_key][0]['model_number'],
                    'barcode' => $option_products[$option_key][0]['barcode'],
                    'discount' => $option_products[$option_key][0]['discount'],
                    'stock' => $option_products[$option_key][0]['amount'],
                ]);

                // check if no product_option_value
                if(!$product_option_value){
                    return [
                        'status' => false,
                        'data' => null,
                        'msg' => 'There is somthing wrong please try again!!'
                    ];
                }

                // get product_option_value_id
                $product_option_value_id = $product_option_value->id;

                // find product_option_values with such id
                $product_opt_values = ProductOptionValues::find($product_option_value_id);

                // check if no product_option_values
                if(!$product_opt_values){
                    return [
                        'status' => false,
                        'data' => null,
                        'msg' => 'There is no product option values with such id!'
                    ];
                }

                // get options_array
                $options_array = $option_products[$option_key][0]['options_array'];

                foreach ($options_array[0]['option_values'] as $opt_key => $opt_array) {
                        $product_opt_values->productOptionValueDetails()->forceCreate([
                          'option_value_id' => $opt_array,
                          'product_option_value_id' => $product_option_value_id
                        ]);
                }

                $product_type = $option_products[$option_key][0]['type'];
                $product_id = $option_products[$option_key][0]['id'];
                $bought_type = $mainBought[0]['bought_type'];
                $user_id = $mainBought[0]['user_id'];

                // check if %bought_id == null
                if($bought_id == null){

                  $amount1 = $option_products[$option_key][0]['amount'];
                  $cost1 = $option_products[$option_key][0]['cost'];
                  $totalPrice1 = $amount1 * $cost1;

                  // store data in Bought
                  $bought = $Bought->forceCreate([
                    'user_id' => $user_id,
                    'total_amount' => $amount1,
                    'total_price' => $totalPrice1,
                    'type' => $bought_type,
                    'paid' => $totalPrice1,
                    'remain' => '0',
                  ]);

                  // check if $bought_type == cache
                  if($bought_type == 'cache'){

                    $bought->paid = $totalPrice1;
                    $bought->remain = 0;

                  }elseif ($bought_type == 'remainder') {

                    $paid = $mainBought[0]['paid'];
                    $bought->paid = $paid;

                    $remain = $totalPrice1 - $paid;
                    $bought->remain = $remain;

                  }

                  // save changed data
                  $bought->save();

                  // get $bought_id
                  $bought_id = $bought->id;

                  // store data in $bought_details
                  $bought_details = $bought->boughtDetails()->forceCreate([
                      'bought_id' => $bought_id,
                      'amount' => $amount1,
                      'cost' => $cost,
                      'product_type' => $product_type,
                      'product_id' => $product_id,
                      'product_option_value_id' => $product_option_value_id,
                  ]);

                }else{

                  $amount = $option_products[$option_key][0]['amount'];
                  $cost = $option_products[$option_key][0]['cost'];
                  $totalPrice = $amount * $cost;

                  $total_amount = $amount + $amount1;
                  $total_price = $totalPrice + $totalPrice1;

                  // find $bought by id
                  $bought = Bought::find($bought_id);

                  // update data in $bought
                  $bought->user_id = $user_id;
                  $bought->total_amount = $total_amount;
                  $bought->total_price = $total_price;
                  $bought->type = $bought_type;

                  // check if $bought_type == cache
                  if($bought_type == 'cache'){

                    $bought->paid = $total_price;
                    $bought->remain = 0;

                  }elseif ($bought_type == 'remainder') {

                    $paid = $mainBought[0]['paid'];
                    $bought->paid = $paid;

                    $remain = $total_price - $paid;
                    $bought->remain = $remain;

                  }

                  // save chacged data
                  $bought->save();

                  // store data in $bought_details
                  $bought_details = $bought->boughtDetails()->forceCreate([
                      'bought_id' => $bought_id,
                      'amount' => $amount,
                      'cost' => $cost,
                      'product_type' => $product_type,
                      'product_id' => $product_id,
                      'product_option_value_id' => $product_option_value_id,
                  ]);

                }
              }
            }
      }
      else if($normal_products){

          // define var normal_amount = 0
          $total_amount = 0;

          // define var total_price = 0
          $total_price = 0;


          foreach ($normal_products as $key => $pro_normal) {

              // get product_id from normal product request
              $product_id = $normal_products[$key][0]['id'];

              // find product for this product_id
              $product = Product::find($product_id);

              // check if no product in such id
              if(!$product){
                  return [
                      'status' => false,
                      'data' => null,
                      'msg' => 'There is no product with such id!'
                  ];
              }

              // get normal_product_details
              $normal_product = NormalProductDetails::where('product_id', $product_id)->first();

              // if normal_product is found
              if($normal_product){

                $stock = $normal_product->stock;

                $amount = $normal_products[$key][0]['amount'];

                $stock = $stock + $amount;

                // update normal product details
                $normal_product->update([
                  'discount_type' => $normal_products[$key][0]['discount_type'],
                  'price' => $normal_products[$key][0]['price'],
                  'serial' => $normal_products[$key][0]['serial'],
                  'model_number' => $normal_products[$key][0]['model_number'],
                  'barcode' => $normal_products[$key][0]['barcode'],
                  'discount' => $normal_products[$key][0]['discount'],
                  'stock' => $stock,
                ]);

                // get all amount for normal product and save them in $normal_amount
                $total_amount += $normal_products[$key][0]['amount'];

                // get prices for normal product
                $normal_price = $normal_products[$key][0]['amount'] * $normal_products[$key][0]['price'];

                // and save them in $total_price
                $total_price += $normal_price;

                continue;

              }else{

                // store data in normal product details table
                $normal_products_details = $product->normalProductDetails()->forceCreate([
                    'product_id' => $product_id,
                    'discount_type' => $normal_products[$key][0]['discount_type'],
                    'price' => $normal_products[$key][0]['price'],
                    'serial' => $normal_products[$key][0]['serial'],
                    'model_number' => $normal_products[$key][0]['model_number'],
                    'barcode' => $normal_products[$key][0]['barcode'],
                    'discount' => $normal_products[$key][0]['discount'],
                    'stock' => $normal_products[$key][0]['amount'],
                ]);

                // check if no normal_product_details
                if(!$normal_products_details){
                    return [
                        'status' => false,
                        'data' => null,
                        'msg' => 'There is no normal product deatils!'
                    ];
                }

                // get all amount for normal product and save them in $normal_amount
                $total_amount += $normal_products[$key][0]['amount'];

                // get prices for normal product
                $normal_price = $normal_products[$key][0]['amount'] * $normal_products[$key][0]['price'];

                // and save them in $total_price
                $total_price += $normal_price;

              }
          }

          // store total_amount
          $Bought->total_amount = $total_amount;

          // store total_price
          $Bought->total_price = $total_price;

          // get bought_type
          $bought_type = $mainBought[0]['bought_type'];

          // check if bought_type == cache
          if($bought_type == 'cache'){

              // store total_price in paid
              $Bought->paid = $total_price;

              // store 0 in remain
              $Bought->remain = '0';

          }else if ($bought_type == 'remainder'){

            // get data in paid
            $bought_paid = $mainBought[0]['paid'];

            // store bought_paid in Bought model
            $Bought->paid = $bought_paid;

            // get bought remain
            $bought_remain = $total_price - $bought_paid;

            // store remain in Bought model
            $Bought->remain = $bought_remain;

          }

          // get user_id
          $user_id = $mainBought[0]['user_id'];

          // store user_id in Bought model
          $Bought->user_id = $user_id;

          // store bought_type in Bought model
          $Bought->type = $bought_type;

          // if Bought save
          if($Bought->save()){

            foreach ($normal_products as $key => $pro_normal) {

                // store bought deatils for bought
                $Bought_Details = $Bought->boughtDetails()->forceCreate([
                  'bought_id' => $Bought->id,
                  'amount' => $normal_products[$key][0]['amount'],
                  'cost' => $normal_products[$key][0]['cost'],
                  'product_type' => $normal_products[$key][0]['type'],
                  'product_id' => $normal_products[$key][0]['id'],
                  'product_option_value_id' => null,
                ]);

                // check if no bought_details
                if(!$Bought_Details){
                    return [
                        'status' => false,
                        'data' => null,
                        'msg' => 'There is no Bought Details!'
                    ];
                }
            }
        }
      }

      // check if status is true
      return [
          'status' => true,
          'data' => null,
          'msg' => 'Boughts has been created successfully!'
      ];
  }


  /**
   * getUpdateBought function
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function getUpdateBought($id)
  {
    // find bought with id
    $main_bought = Bought::find($id);

    //check if no bought
    if(!$main_bought)
    {
      // id status is false
      return [
        'status' => false,
        'data' => null,
        'msg' => 'There is no Bought in such id!'
      ];
    }

    // dd($main_bought);
    // get user_id
    $user_id = $main_bought->user_id;

    // get user_name for this user_id
    $user_name = User::where('id', $user_id)->first()->name;

    // get bought details
    $bought_details = $main_bought->boughtDetails()->get();

    // append bought_details
    foreach ($bought_details as $key => $bought) {

      // get product_id
      $product_id = $bought->product_id;

      // get product name for this product_id
      $product_name = ProductTranslation::where('product_id', $product_id)->first()->name;

      // append productName to bought
      $bought->productName = $product_name;

      // get product_option_value_id
      $product_option_value_id = $bought->product_option_value_id;

      // get product_option_value for this product_option_value_id
      $product_option_value = ProductOptionValues::find($product_option_value_id);

      // append ProductOptionValue to bought
      $bought->ProductOptionValue = $product_option_value;

      // get normal_product_details for this normal_product_details_id
      $normal_product_details = NormalProductDetails::where('product_id', $product_id)->first();

      // append NormalProductDetails to bought
      $bought->NormalProductDetails = $normal_product_details;

      // get option_value_ids
      $option_value_ids = ProductOptionValuesDetails::where('product_option_value_id', $product_option_value_id)->get(['option_value_id']);

      // append option_value_ids to bought
      $bought->option_value_ids = $option_value_ids;

      // append option_value_id
      foreach ($option_value_ids as $key => $opt_val_id) {

        // get option_value_id
        $option_value_id = $opt_val_id->option_value_id;

        // find option_value for this id
        $option_value = OptionValues::find($option_value_id);

        // get option_id
        $option_id = $option_value->option_id;

        // append option_id to opt_val_id
        $opt_val_id->option_id = $option_id;

        // find option with this id
        $option = Option::find($option_id);

        // get option_name
        $option_name = $option->optionTrans()->first()->option;

        // append option_name
        $opt_val_id->option_name = $option_name;

        // get option_value
        $option_value = $option_value->optionValuesTrans()->first()->value;

        // append option_value
        $opt_val_id->option_value = $option_value;
      }
    }

    // dd($bought_details);

    // get role_id  where role = vendor
    $role_id = Role::where('role','vendor')->first()->id;

    // get all users that wit role vendor
    $users = User::where('role_id', $role_id)->get();

    // get all products
    $products = Product::all();

    // append translated products
    foreach ($products as $product) {

        // get product details
        $product->trans = $product->translate();
    }

    // get all Options
    $options = Option::all();

    // append translated option
    foreach ($options as $option) {

        // get option details
        $option->trans = $option->translate();
        $option->values = $option->optionValues;

        foreach($option->values as $value) {
            $value->trans = $value->translate();
        }
    }


    return [
        'status' => true,
        'data' => [
          'main_bought' => $main_bought,
          'user_name' => $user_name,
          'users' => $users,
          'option_value_ids' => $option_value_ids,
          'products' => $products,
          'options' => $options,
          'bought_details' => $bought_details
        ],
        'msg' => 'Boughts has been successfully displayed!'
    ];
  }


  /**
    * updateBought function
    * @param Request $request
    * @param $id
    * @return array
    */
    public function updateBought(Request $request, $id)
    {
      $validation_boughts = [

      ];

      $validation = validator($request->all(), $validation_boughts);

      // if validation failed, return false response
      if ($validation->fails()) {
        foreach ($validation->errors()->all() as $key => $value) {
          return [
              'status' => false,
              'data' => 'validation error',
              'msg' => $value
          ];
        }
      }
      // get main Bought from request
      $mainBought = $request->mainBought;

      // get normal Product from request
      $normal_products = $request->normal_products;

      // get option Products from request
      $option_products = $request->option_products;

      // get bought with such id
      $bought = Bought::find($id);

      // get bought Details
      $bought_details = $bought->boughtDetails()->get();

      $bought_details_id = $request->Bought_Details_Id;

      if($bought_details_id){

        $bought_detail = $bought->boughtDetails()->where('id', $bought_details_id)->first();

        $product_type = $bought_detail->product_type;

        if($product_type == 'option'){

          $product_option_value_id = $request->Id;

          $product_option_value = ProductOptionValues::find($product_option_value_id);

          if($product_option_value){

            foreach ($option_products as $option_key => $opt_pro) {

              $product_id = $opt_pro['id'];

              // get option values ids (2d array)
              $opt_val_ids = ProductOptionValuesDetails::where('product_option_value_id', $product_option_value_id)->get(['option_value_id'])->toArray();

              // covert 2d array to 1d array
              $product_option_values_details_ids = array_map('current', $opt_val_ids);

              // check if no $product_option_values_details_ids
              if(!$product_option_values_details_ids){
                return [
                  'status' => false,
                  'data' => null,
                  'msg' => 'There is no product option value details!'
                ];
              }

              // get option values
              $option_values = [0]['option_values'];

              // check if two array equals
              if(array_diff($product_option_values_details_ids, $option_values) == null){

                $stock = $product_option_value->stock;
                // get stock
                $amount = $option_products[$option_key]['amount'];

                $stock = $stock + $amount;

                $product_option_value->price = $option_products[$option_key]['price'];
                $product_option_value->serial = $option_products[$option_key]['serial'];
                $product_option_value->model_number = $option_products[$option_key]['model_number'];
                $product_option_value->barcode = $option_products[$option_key]['barcode'];
                $product_option_value->discount = $option_products[$option_key]['discount'];
                $product_option_value->stock = $stock;

                $product_option_value->save();

                $cost = $option_products[$option_key]['cost'];

                $bought_details_id = $details->id;
                $boughtDeatils = BoughtDetails::find($bought_details_id);

                $boughtDeatils->amount = $amount;
                $boughtDeatils->cost = $cost;

                // $total_amount = $amount;
                // $total_price = $amount * $cost;

                continue;
            }else{

              // get options_array
              $options_array = $option_products[$option_key]['options_array'];

              // get option values
              $option_values = $options_array[0]['option_values'];

              foreach ($option_values as $opt_key => $opt_array) {

                $product_option_value_details = $product_option_value->productOptionValueDetails();
                $product_option_value_details->option_value_id = $opt_array;
                $product_option_value_details->product_option_value_id = $product_option_value_id;

              }


              $stock = $product_option_value->stock;
              // get stock
              $amount = $option_products[$option_key]['amount'];

              $stock = $stock + $amount;

              // store data in product_option_value
              $product_option_value->price = $option_products[$option_key]['price'];
              $product_option_value->serial = $option_products[$option_key]['serial'];
              $product_option_value->model_number = $option_products[$option_key]['model_number'];
              $product_option_value->barcode = $option_products[$option_key]['barcode'];
              $product_option_value->discount = $option_products[$option_key]['discount'];
              $product_option_value->stock = $stock;

              $product_option_value->save();

              $cost = $option_products[$option_key]['cost'];

              $bought_details_id = $details->id;
              $boughtDeatils = BoughtDetails::find($bought_details_id);

              $boughtDeatils->amount = $amount;
              $boughtDeatils->cost = $cost;

              // $total_amount = $amount;
              // $total_price = $amount * $cost;

              }
            }
          }else{

            // get options array
            $options_array = $option_products[$option_key]['options_array'];

            // get product_id
            $product_id =$option_products[$option_key]['id'];

            // find product for this product_id
            $product = Product::find($product_id);

            // check if no product
            if(!$product){
                return [
                    'status' => false,
                    'data' => null,
                    'msg' => 'There is no product with such id!'
                ];
            }

            // store data in product_option_value
            $product_option_value = $product->productOptionValue()->forceCreate([
                'product_id' => $product_id,
                'price' => $option_products[$option_key]['price'],
                'serial' => $option_products[$option_key]['serial'],
                'model_number' => $option_products[$option_key]['model_number'],
                'barcode' => $option_products[$option_key]['barcode'],
                'discount' => $option_products[$option_key]['discount'],
                'stock' => $option_products[$option_key]['amount'],
            ]);

            // check if no product_option_value
            if(!$product_option_value){
                return [
                    'status' => false,
                    'data' => null,
                    'msg' => 'There is somthing wrong please try again!!'
                ];
            }

            // get product_option_value_id
            $product_option_value_id = $product_option_value->id;

            // find product_option_values with such id
            $product_opt_values = ProductOptionValues::find($product_option_value_id);

            // check if no product_option_values
            if(!$product_opt_values){
                return [
                    'status' => false,
                    'data' => null,
                    'msg' => 'There is no product option values with such id!'
                ];
            }

            // get options_array
            $options_array = $option_products[$option_key]['options_array'];

            foreach ($options_array[0]['option_values'] as $opt_key => $opt_array) {
                    $product_opt_values->productOptionValueDetails()->forceCreate([
                      'option_value_id' => $opt_array,
                      'product_option_value_id' => $product_option_value_id
                    ]);
            }

            $amount = $deatils->amount;
            $amount += $option_products[$option_key]['amount'];
            $cost = $deatils->cost;
            $cost += $option_products[$option_key]['cost'];
          }
        }
        elseif ($product_type == 'normal') {

          $total_amount = 0;
          $total_price = 0;

          $normal_product_details_id = $request->Id;

          $normal_product_details = NormalProductDetails::find($normal_product_details_id);

          if($normal_product_details){

            foreach ($normal_products as $key => $normal_pro) {

              $amount = $normal_products[$key][0]['amount'];
              $cost = $normal_products[$key][0]['cost'];
              $stock = $normal_product_details->stock;
              $stock += $amount;

              $normal = $normal_product_details->update([
                'product_id' => $normal_products[$key][0]['id'],
                'discount_type' => $normal_products[$key][0]['discount_type'],
                'price' => $normal_products[$key][0]['price'],
                'serial' => $normal_products[$key][0]['serial'],
                'model_number' => $normal_products[$key][0]['model_number'],
                'barcode' => $normal_products[$key][0]['barcode'],
                'discount' => $normal_products[$key][0]['discount'],
                'stock' => $stock,
              ]);

              $bought_detail->amount = $amount;
              $bought_detail->cost = $cost;

              $bought_detail->save();

              //total amount
              $total_amount += $amount;
              // get prices
              $prices = $amount * $cost;
              // total_price
              $total_price += $prices;


              $bought->total_amount = $total_amount;
              $bought->total_amount = $total_price;

              $bought->save();

            }

          }else{
            // get product_id from normal product request
            $product_id = $normal_products[$key][0]['id'];

            // find product for this product_id
            $product = Product::find($product_id);

            // check if no product in such id
            if(!$product){
                return [
                    'status' => false,
                    'data' => null,
                    'msg' => 'There is no product with such id!'
                ];
            }

            // store data in normal product details table
            $normal_products_details = $product->normalProductDetails()->forceCreate([
                'product_id' => $product_id,
                'discount_type' => $normal_products[$key][0]['discount_type'],
                'price' => $normal_products[$key][0]['price'],
                'serial' => $normal_products[$key][0]['serial'],
                'model_number' => $normal_products[$key][0]['model_number'],
                'barcode' => $normal_products[$key][0]['barcode'],
                'discount' => $normal_products[$key][0]['discount'],
                'stock' => $normal_products[$key][0]['amount'],
            ]);

            // check if no normal_product_details
            if(!$normal_products_details){
                return [
                    'status' => false,
                    'data' => null,
                    'msg' => 'There is no normal product deatils!'
                ];
            }

            $bought_detail = $bought->boughtDetails()->forceCreate([
              'bought_id' => $bought->id,
              'amount' => $normal_products[$key][0]['amount'],
              'cost' => $normal_products[$key][0]['cost'],
              'product_type' => $normal_products[$key][0]['product_type'],
              'product_id' => $normal_products[$key][0]['product_id'],
              'product_option_value_id' => null,
            ]);
          }
        }
      }

      // check if status is true
      return [
          'status' => true,
          'data' => null,
          'msg' => 'Bought`s has been updated successfully!'
      ];
    }


  /**
    * deleteBought function
    * @param $id
    * @return array
    */
    public function deleteBought($id)
    {
      //search category by id
      $bought = Bought::find($id);

      // check if no category
      if (!$bought) {
          return [
              'status' => false,
              'data' => null,
              'msg' => 'There is no bought with this id!!'
          ];
      }

      //delete bought details
      $bought->boughtDetails()->delete();

      // delete bought
      $bought->delete();

      //check successfully deleted data
      return [
          'status' => true,
          'data' => null,
          'msg' => 'Boughts has been Deleted Successfully!'
      ];

    }

    /**
     * @param $id
     * @return array
     */
    public function deleteProductBought($id)
    {
      //search bought_details by id
      $bought_details = BoughtDetails::find($id);

      // check if no bought_details
      if (!$bought_details) {
          return [
              'status' => false,
              'data' => null,
              'msg' => 'There is no bought details with this id!!'
          ];
      }

      // bought_details delete
      $bought_details->delete();

      //check successfully deleted data
      return [
          'status' => true,
          'data' => null,
          'msg' => 'Boughts has been successfully deleted!'
      ];
    }

}
