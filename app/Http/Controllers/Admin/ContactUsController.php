<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\ContactUsControllerApi;


class ContactUsController extends Controller
{
  /**
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function getIndex()
  {
      $contactUs_api = new ContactUsControllerApi();

      $getIndex_api = $contactUs_api->getIndex();

      if($getIndex_api['status'] == false)
      {
          return [
              'status' => 'error',
              'title' => 'Error',
              'text' => $getIndex_api['msg']
          ];
      }

      return view('admin.pages.contactUs.index');
  }
}
