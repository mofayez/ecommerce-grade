<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Image;

class ImageController extends Controller
{

    /**
    * delete an image
    * @param int
    */
    public function deleteImage(int $image_id) {
        
        Image::find($image_id)->delete();
    }
}
