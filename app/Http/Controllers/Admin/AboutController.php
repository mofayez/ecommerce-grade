<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\AboutControllerِApi;

class AboutController extends Controller
{
  /**
  * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
  */
  public function getIndex()
  {
    $about_api = new AboutControllerِApi();

    $getIndex_api = $about_api->getIndex();

    return view('admin.pages.aboutUs.index');
  }

  /**
  * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
  */
  public function getCreateNewAbout()
  {
    $about_api = new AboutControllerِApi();

    $getCreateNewAbout_api = $about_api->getCreateNewAbout();

    return view('admin.pages.aboutUs.add-about');
  }


}
