<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\FAQsControllerApi;

class FAQsController extends Controller
{
  /**
  * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
  */
  public function getIndex()
  {
    $faqs_api = new FAQsControllerApi();

    $getIndex_api = $faqs_api->getIndex();

    return view('admin.pages.FAQs.index', $getIndex_api['data']);
  }

  /**
  * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
  */
  public function getCreateNewFaqs()
  {
    $faqs_api = new FAQsControllerApi();

    $getCreateNewFaqs_api = $faqs_api->getCreateNewFaqs();

    return view('admin.pages.FAQs.add-faqs');
  }

  /**
   * @param Request $request
   * @return array
   */
  public function createNewFaqs(Request $request)
  {
    $faqs_api = new FAQsControllerApi();

    $createNewFaqs_api = $faqs_api->createNewFaqs($request);

    if($createNewFaqs_api['status'] == false)
    {
        return [
            'status' => 'error',
            'title' => 'Error',
            'text' => $createNewFaqs_api['msg']
        ];
    }

    // check saving success
    return [
        'status' => 'success',
        'title' => 'success',
        'text' => $createNewFaqs_api['msg']
    ];
  }


  /**
  * @param $id
  * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
  */
  public function getUpdateFaqs($id)
  {
    $faqs_api = new FAQsControllerApi();

    $getUpdateFaqs_api = $faqs_api->getUpdateFaqs($id);

    if($getUpdateFaqs_api['status'] == false)
    {
        return [
            'status' => 'error',
            'title' => 'Error',
            'text' => $getUpdateFaqs_api['msg']
        ];
    }

    return view('admin.pages.FAQs.edit-faqs', $getUpdateFaqs_api['data']);
  }


  /**
  * @param $id
  * @param Request $request
  * @return array
  */
  public function updateFaqs(Request $request, $id)
  {
    $faqs_api = new FAQsControllerApi();

    $updateFaqs_api = $faqs_api->updateFaqs($request, $id);

    if($updateFaqs_api['status'] == false)
    {
        return [
            'status' => 'error',
            'title' => 'Error',
            'text' => $updateFaqs_api['msg']
        ];
    }

    // check saving success
    return [
        'status' => 'success',
        'title' => 'success',
        'text' => $updateFaqs_api['msg']
    ];
  }



      /**
      * @param $id
      * @return array
      */
      public function deleteFaqs($id)
      {
        $faqs_api = new FAQsControllerApi();

        $deleteFaqs_api = $faqs_api->deleteFaqs($id);

        if($deleteFaqs_api['status'] == false)
        {
            return [
                'status' => 'error',
                'title' => 'Error',
                'text' => $deleteFaqs_api['msg']
            ];
        }

        // check saving success
        return [
            'status' => 'success',
            'title' => 'success',
            'text' => $deleteFaqs_api['msg']
        ];

      }

}
