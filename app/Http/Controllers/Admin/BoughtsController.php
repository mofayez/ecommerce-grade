<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\Bought;
use App\Models\ProductOptionValues;
use App\Models\Option;
use App\Models\OptionValues;
use App\Models\ProductTranslation;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BoughtsControllerApi;

/**
  * class BoughtController
  * @package App/Http/Controllers/Admin
  */
class BoughtsController extends Controller
{
    /**
      * getIndex function
      * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
      */
    public function getIndex()
    {
        $boughts_api = new BoughtsControllerApi();

        $getIndex_api = $boughts_api->getIndex();

        if($getIndex_api['status'] == false){
          return [
            'status' => 'error',
            'title' =>'Error',
            'text' => $getIndex_api['msg']
          ];
        }

        return view('admin.pages.boughts.index', $getIndex_api['data']);
    }

    /**
      * getCreateNewBought function
      * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
      */
    public function getCreateNewBought(Request $request)
    {
        $boughts_api = new BoughtsControllerApi();

        $getCreateNewBought_api = $boughts_api->getCreateNewBought($request);

        if($getCreateNewBought_api['status'] == false)
        {
          return [
            'status' => 'error',
            'title' => 'Error',
            'text' => $getCreateNewBought_api['msg']
          ];
        }

        return view('admin.pages.boughts.add-bought',$getCreateNewBought_api['data']);
    }


    /**
      * getBoughtSectionView function
      * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
      */
    public function getBoughtSectionView()
    {
        $boughts_api = new BoughtsControllerApi();

        $getBoughtSectionView_api = $boughts_api->getBoughtSectionView();

        if($getBoughtSectionView_api['status'] == false)
        {
          return [
            'status' => 'error',
            'title' => 'Error',
            'text' => $getBoughtSectionView_api['msg'],
          ];
        }

        return view('admin.pages.boughts.templates.bought-section', $getBoughtSectionView_api['data'])->render();
    }

    /**
      * getBoughtSectionView function
      * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
      */
    public function getUpdateBoughtSectionView()
    {
        $boughts_api = new BoughtsControllerApi();

        $getUpdateBoughtSectionView_api = $boughts_api->getUpdateBoughtSectionView();

        if($getUpdateBoughtSectionView_api['status'] == false)
        {
          return [
            'status' => 'error',
            'title' => 'Error',
            'text' => $getUpdateBoughtSectionView_api['msg'],
          ];
        }

        return view('admin.pages.boughts.templates.update-bought-section', $getUpdateBoughtSectionView_api['data'])->render();
    }

    /**
      * getOptionSectionView function
      * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
      */
    public function getOptionSectionView()
    {
      $boughts_api = new BoughtsControllerApi();

      $getOptionSectionView_api = $boughts_api->getOptionSectionView();

      if($getOptionSectionView_api['status'] == false)
      {
        return [
          'status' => 'error',
          'title' => 'Error',
          'text' => $getOptionSectionView_api['msg'],
        ];
      }

        return view('admin.pages.boughts.templates.option-section', $getOptionSectionView_api['data'])->render();
    }

    /**
    * get option's values
    */
    public function getOptionValues($id)
    {
      $boughts_api = new BoughtsControllerApi();

      $getOptionValues_api = $boughts_api->getOptionValues($id);

      if($getOptionValues_api['status'] == false)
      {
        return [
          'status' => 'error',
          'title' => 'Error',
          'text' => $getOptionValues_api['msg'],
        ];
      }

        return view('admin.pages.boughts.templates.option-values', $getOptionValues_api['data'])->render();
    }

    /**
    * get option's values
    */
    public function getUpdateOptionValues($id)
    {
      $boughts_api = new BoughtsControllerApi();

      $getUpdateOptionValues_api = $boughts_api->getUpdateOptionValues($id);

      if($getUpdateOptionValues_api['status'] == false)
      {
        return [
          'status' => 'error',
          'title' => 'Error',
          'text' => $getUpdateOptionValues_api['msg'],
        ];
      }

        return view('admin.pages.boughts.templates.update-option-values', $getUpdateOptionValues_api['data'])->render();
    }

    /**
      * createNewBought function
      *  @param Request request
      *  @return array
      */
    public function createNewBought(Request $request)
    {

      $boughts_api = new BoughtsControllerApi();

      $createNewBought_api = $boughts_api->createNewBought($request);

      if($createNewBought_api['status'] == false)
      {
        return [
          'status' => 'error',
          'title' => 'Error',
          'text' => $createNewBought_api['msg'],
        ];
      }

      return [
          'status' => 'success',
          'title' => 'success',
          'text' => $createNewBought_api['msg']
      ];
    }

    /**
     * getUpdateBought function
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdateBought($id)
    {
      $boughts_api = new BoughtsControllerApi();

      $getUpdateBought_api = $boughts_api->getUpdateBought($id);

      if($getUpdateBought_api['status'] == false)
      {
        return [
          'status' => 'error',
          'title' => 'Error',
          'text' => $getUpdateBought_api['msg'],
        ];
      }

      return view('admin.pages.boughts.edit-bought', $getUpdateBought_api['data']);
    }

    /**
      * updateBought function
      * @param Request $request
      * @param $id
      * @return array
      */
      public function updateBought(Request $request, $id)
      {
        $boughts_api = new BoughtsControllerApi();

        $updateBought_api = $boughts_api->updateBought($request, $id);

        if($updateBought_api['status'] == false)
        {
          return [
            'status' => 'error',
            'title' => 'Error',
            'text' => $updateBought_api['msg'],
          ];
        }

        return [
            'status' => 'success',
            'title' => 'success',
            'text' => $updateBought_api['msg']
        ];
      }


    /**
      * deleteBought function
      * @param $id
      * @return array
      */
      public function deleteBought($id)
      {
        $boughts_api = new BoughtsControllerApi();

        $deleteBought_api = $boughts_api->deleteBought($id);

        if($deleteBought_api['status'] == false)
        {
          return [
            'status' => 'error',
            'title' => 'Error',
            'text' => $deleteBought_api['msg'],
          ];
        }

        return [
            'status' => 'success',
            'title' => 'success',
            'text' => $deleteBought_api['msg']
        ];
      }


      /**
       * @param $id
       * @return array
       */
      public function deleteProductBought($id)
      {
        $boughts_api = new BoughtsControllerApi();

        $deleteProductBought_api = $boughts_api->deleteProductBought($id);

        if($deleteProductBought_api['status'] == false)
        {
          return [
            'status' => 'error',
            'title' => 'Error',
            'text' => $deleteProductBought_api['msg'],
          ];
        }
        //check successfully deleted data
        return [
            'status' => 'success',
            'data' => 'Success',
            'msg' => $deleteProductBought_api['msg']
        ];
      }


}
