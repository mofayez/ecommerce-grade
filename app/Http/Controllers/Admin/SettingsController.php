<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\SettingsControllerApi;

class SettingsController extends Controller
{
  /**
  * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
  */
  public function getIndex()
  {
    $settings_api = new SettingsControllerApi();

    $getIndex_api = $settings_api->getIndex();

    return view('admin.pages.settings.index', $getIndex_api['data']);
  }

  /**
  * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
  */
  public function getCreateNewSettings()
  {
    $settings_api = new SettingsControllerApi();

    $getCreateNewSettings_api = $settings_api->getCreateNewSettings();

    return view('admin.pages.settings.add-settings');
  }


  /**
   * @param Request $request
   * @return array
   */
  public function createNewSettings(Request $request)
  {
    $settings_api = new SettingsControllerApi();

    $createNewSettings_api = $settings_api->createNewSettings($request);

    if($createNewSettings_api['status'] == false)
    {
        return [
            'status' => 'error',
            'title' => 'Error',
            'text' => $createNewSettings_api['msg']
        ];
    }

    //check success status
    return [
        'status' => 'success',
        'title' => 'success',
        'text' => $createNewSettings_api['msg']
    ];
  }

  /**
  * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
  */
  public function getUpdateSettings($id)
  {
    $settings_api = new SettingsControllerApi();

    $getUpdateSettings_api = $settings_api->getUpdateSettings($id);

    if($getUpdateSettings_api['status'] == false)
    {
        return [
            'status' => 'error',
            'title' => 'Error',
            'text' => $getUpdateSettings_api['msg']
        ];
    }

    //return view edit-user page
    return view('admin.pages.settings.edit-settings', $getUpdateSettings_api['data']);
  }



  /**
   * @param $id
   * @param Request $request
   * @return array
   */
  public function updateSettings(Request $request, $id)
  {
    $settings_api = new SettingsControllerApi();

    $updateSettings_api = $settings_api->updateSettings($request, $id);

    if($updateSettings_api['status'] == false)
    {
        return [
            'status' => 'error',
            'title' => 'Error',
            'text' => $updateSettings_api['msg']
        ];
    }

    //check success status
    return [
        'status' => 'success',
        'title' => 'success',
        'text' => $updateSettings_api['msg']
    ];
  }

  /**
   * @param $id
   * @return array
   */
  public function deleteSettings($id)
  {
    $settings_api = new SettingsControllerApi();

    $deleteSettings_api = $settings_api->deleteSettings($id);

    if($deleteSettings_api['status'] == false)
    {
        return [
            'status' => 'error',
            'title' => 'Error',
            'text' => $deleteSettings_api['msg']
        ];
    }

    //check delete successfully
    return [
        'status' => 'success',
        'title' => 'Success',
        'text' => $deleteSettings_api['msg'],
    ];
  }
}
