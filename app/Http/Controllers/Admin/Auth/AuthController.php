<?php
namespace App\Http\Controllers\Admin\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{

    /**
    * get login page
    */
    public function getLogin()
    {
        // return login view
        return view('admin.auth.login');
    }

    /**
    * post login
    */
    public function postLogin(Request $request)
    {


        // validation rules
        $rules = [
            'email' => 'required',
            'password' => 'required'
        ];

        // validate
        $validation = validator($request->all(), $rules);

        // check validation conclusion
        if ($validation->fails()) {

            // return back to login page with err msgs and the old inputs
            return back()->withErrors($validation->getMessageBag())->withInput($request);
        }

        // excreact request data
        $email = $request->email;
        $password = $request->password;
        $remember = $request->remember;

        // grape user data
        $user = User::where('email', $email)->first();

        // check if not user exist
        if (!$user || $user->role->role != 'admin') {

            // return back with error msgs
            return back()->withErrors(['email' => 'Email not exist!']);
        }

        // get user's password
        $user_password = $user->password;

        // check if password entered by the user is correct
        if (!Hash::check($password, $user_password)) {

            // return back with error msgs
            return back()->withErrors(['password' => 'Icorrect password!']);
        }

        // login user
        Auth::login($user, $remember);

        // redirect to home page
        return redirect()->route('admin.home');
    }


    /**
    * hane logout
    */
    public function getLogout()
    {
        // check if there is a user authenticated
        if (!Auth::check()) {

            return back();
        }

        // logout user
        Auth::logout();

        // return login page
        return redirect()->route('admin.getLogin');
    }



}
