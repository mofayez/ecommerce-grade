<?php

namespace App\Http\Controllers\Site\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    // get login / register page
    public function getLogin()
    {
        // return login / register view
        return view('site.auth.login');
    }

    /**
    * post login
    */
    public function postLogin(Request $request)
    {

        // validation rules
        $rules = [
            'email' => 'required',
            'password' => 'required'
        ];

        // validate
        $validation = validator($request->all(), $rules);

        // check validation conclusion
        if ($validation->fails()) {

            // return back to login page with err msgs and the old inputs
            return back()
                    ->withErrors($validation->getMessageBag())
                    ->withInput($request->all());
        }

        // excreact request data
        $email = $request->email;
        $password = $request->password;
        $remember = $request->remember;

        // grape user data
        $user = User::where('email', $email)->first();

        // check if not user exist
        if (!$user || $user->role->role != 'client') {

            // return back with error msgs
            return back()->withErrors(['email' => 'Email not exist!']);
        }

        // get user's password
        $user_password = $user->password;

        // check if password entered by the user is correct
        if (!Hash::check($password, $user_password)) {


            // return back with error msgs
            return back()->withErrors(['password' => 'Icorrect password!']);
        }

        // login user
        Auth::login($user, $remember);

        // redirect to home page
        return redirect()->route('site.home.index');
    }

    /**
    * register new user
    */
    public function postRegister(Request $request)
    {
        // validation rules used in validation process
        $validation_rules = [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|confirmed',
            'phone' => 'required',
            'address' => 'required',
            'postal' => 'required'
        ];

        // make validation
        $validation = validator($request->all(), $validation_rules, [
            'postal.required' => 'postal code field is required'
        ]);

        // check validation result
        if ($validation->fails()) {

            // return back to register page with validation rules and old inputs
            return back()
                    ->withErrors($validation->getMessageBag())
                    ->withInput($request->all());
        }

        // get client rule id
        $client_rule_id = Role::where('role', 'client')->first()->id;

        // store client in the database
        $user = User::forceCreate([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'mobile' => $request->phone,
            'phone' => $request->phone,
            'role_id' => $client_rule_id,
            'address_1' => $request->address,
            'gender' => $request->gender,
            'postal_code' => $request->postal
        ]);

        // if something accedently went wrong
        if (!$user) {

            return back();
        }

        // authenticate user
        Auth::login($user);

        // redirect to home page
        return redirect()->route('site.home.index');
    }

}
