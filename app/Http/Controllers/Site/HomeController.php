<?php

namespace App\Http\Controllers\Site;

use App\Models\Menu;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    // get home page
    public function index()
    {

        // get all menues
        $menus = Menu::all();

        foreach ($menus as $menu) {

            // get menu trans
            $menu->trans = $menu->translate();

            // get menu's main categories
            $categories = $menu->categories()->where('parent_id', 0)->get();

            // translate categories
            foreach ($categories as $category) {

                // get category details
                $category_translated = $category->translate();

                // add the translated Category as a key => value to main Category object
                // key is category_translated and the value id $category_translated
                $category->trans = $category_translated;


                // get sub categories
                $sub_categories = Category::where('parent_id', $category->id)->get();

                //translate sub categories
                foreach ($sub_categories as $sub_category) {

                    $sub_category->trans = $sub_category->translate();
                }

                // apend sub categories to main ones
                $category->sub_categories = $sub_categories;
            }

            $menu->categories = $categories;
        }

        // get all products
        $products = Product::all();

        // translate all products
        foreach ($products as $product) {

            $product->trans = $product->translate();
        }

        // shuffle products
        $products = $products->shuffle();

        // take 10 products
        $products = $products->take(10);

        return view('site.pages.index', compact('menus', 'products'));
    }
}
