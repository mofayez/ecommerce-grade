<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductOptionValues;

class ProductController extends Controller
{

    // get productOptionValue price
    public function getProductOptionValue (Request $request)
    {
        $product_option_value_id = $request->product_option_value_id;

        return ProductOptionValues::find($product_option_value_id)->price;
    }

}
