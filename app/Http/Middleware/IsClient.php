<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class IsClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // check if user is authenticated
        if (Auth::check()) {

            // get authenticated user
            $user = Auth::user();

            // get users's role
            $role = $user->role->role;

            // check if user is admin
            if ($role == 'client') {

                // handle request
                return $next($request);
            }
        }

        // return to lohin page
        return redirect()->route('site.getLogin');
    }
}
