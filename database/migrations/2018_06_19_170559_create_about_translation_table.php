<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_translation', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('about_id')->unsigned();
            $table->foreign('about_id')->references('id')->on('abouts')->onDelete('cascade');

            $table->integer('lang_id')->unsigned();
            $table->foreign('lang_id')->references('id')->on('languages')->onDelete('cascade');

            $table->string('title');
            $table->text('content');
            $table->string('image');
            $table->string('Call_title');
            $table->string('Call_icon');
            $table->string('Call_value');

            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_translation');
    }
}
