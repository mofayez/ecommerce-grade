<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('phone')->nullable();
            $table->integer('mobile')->nullable();
            $table->string('email')->unique();
            $table->string('gender')->nullable();
            $table->string('address-1')->nullable();
            $table->string('address-2')->nullable();
            $table->string('logo')->unique();
            $table->text('meta_descriptions')->nullable();
            $table->string('title')->nullable();
            $table->text('about')->nullable();

            $table->softDeletes();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
