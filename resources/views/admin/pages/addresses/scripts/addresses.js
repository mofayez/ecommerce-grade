$(document).ready(function() {

    // var add_cities_template = '';

    var add_city_button = $('.add_new_city');

    add_city_button.on('click', function(event) {
        event.preventDefault();
        var url = $(this).data('route');
        $.ajax({
            url: url,
            method: 'GET',
            success: function(result) {
                $('.add_cities_section').append(result);
            }
        });
    });

    $(document).on('click', '.remove_city', function(event) {
        event.preventDefault();
        $(this).closest('.city_wrapper').remove();

    });

});



$(document).ready(function() {

    var add_cities_template = '';

    $.ajax({
        url: $('.add_cities').data('url'),
        method: 'GET',
        async: false,
        success: function(result) {
            add_cities_template += result;
        }
    });

    $('.add_cities').on('click', function(event) {

        event.preventDefault();

        $('.add_cities_section').append(add_cities_template);

    });

    $(document).on('click', '.remove_city', function(event) {

        event.preventDefault();

        var _this = $(this);

        if (_this.data('old') == 'n') {

            _this.closest('.city_wrapper').remove();
        } else {

            swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this imaginary file!",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {

                  $.ajax({
                      url: _this.data('url'),
                      method: 'GET',
                      success: function() {
                          swal("Poof! Your imaginary file has been deleted!", {
                            icon: "success",
                          });
                          _this.closest('.city_wrapper').remove();
                      }
                  });

              } else {
                swal("Your imaginary file is safe!");
              }
            });
        }
    });
});


// function update option Value
$(document).on('click', '.btn-update-country-submit', function (e) {

    e.preventDefault();

    var cities_en = [];
    var cities_ar = [];

    var inputs_en = $("input[name='city_name_en[]']");

    inputs_en.each(function (key, item) {

     if ($(this).val()) {

         var value = $(this).val();

         var city_id =  $(this).data('cityid');

         cities_en.push([value, city_id]);
     }

    });


    var inputs_ar = $("input[name='city_name_ar[]']");

    inputs_ar.each(function (key, item) {

        if ($(this).val()) {
            var value = $(this).val();

            var city_id =  $(this).data('cityId');

            cities_ar.push([value, city_id]);
        }

    });

    var country_ar = $("input[name='country_name_ar']").val();
    var country_en = $("input[name='country_name_en']").val();
    var country_code = $("input[name='country_code']").val();


    var action_url = $(this).closest('form').attr('action');

    $.ajax({
      url: action_url,
      method: 'POST',
      data: {
        country_id: $("input[name='country_id']").val(),
        country_name_ar: country_ar,
        country_name_en: country_en,
        country_code: country_code,
        cities_names_en: cities_en,
        cities_names_ar: cities_ar,
      },
      success: function (data) {
          // console.log(data);
          if (data.status == 'success') {
            new Noty({
                layout   : 'topRight',
                type     : 'success',
                theme    : 'relax',
                dismissQueue: true,
                timeout  : 1500,
                text     : [data.text],
            }).show()

            setTimeout(function() {
                location.reload(0);
            }, 2000);
          } else {
              //error code...
              new Noty({
                  layout   : 'topRight',
                  type     : 'error',
                  theme    : 'relax',
                  timeout: 1500,
                  text: [data.text],
              }).show();
          }
      },
      error: function (data) {
        new Noty({
            layout   : 'topRight',
            type     : 'error',
            theme    : 'relax',
            timeout: 1500,
            text: [data.text],
        }).show();
      }
    });
});
