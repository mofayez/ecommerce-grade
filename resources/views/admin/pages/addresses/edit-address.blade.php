@extends('admin.master')

@section('styles')

<style>

input[name='city_name_ar[]'],  input[name='country_name_ar']{
    text-align: right;
}

</style>

@endsection

@section('content-header')
    <section class="content-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><h4>Addresses</h4></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.home')  }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.addresses.getIndex')  }}">Addresses</a></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.addresses.getUpdateAddress', [ 'id' => $country->id])}}">Edit Addresses</a>
                </li>
            </ol>
        </nav>
    </section>
@endsection

@section('content')

    <section class="content">
        <div class="panel panel-primary">
            <!-- Default panel contents -->
            <div class="panel-heading" style="font-size: large">Address</div>

            <form action="{{route('admin.addresses.updateAddress')}}" class="update-form" enctype="multipart/form-data"
                  method="post"
                  onsubmit="return false;">
                {!! csrf_field() !!}

                <input type="hidden" name="country_id" value="{{$country->id}}">

                <div class="modal-body">

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="country_name_en" class="col-2 col-form-label ">Country name in EN</label>
                            <div class="col-10">
                                <input class="form-control required" type="text" name="country_name_en" value="{{$country->en->country}}"
                                       placeholder="EX: Egypt">
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="country_name_ar" class="col-2 col-form-label" style="float: right;">اسم الدولة باللغة العربيه</label>
                            <div class="col-10">
                                <input class="form-control required" type="text" name="country_name_ar" value="{{$country->ar->country}}"
                                       placeholder="مثلا: مصر">
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="country_code" class="col-2 col-form-label">Country Code</label>
                            <div class="col-10">
                                <input class="form-control required" type="text" id="country_code" name="country_code" value="{{$country->country_code}}"
                                       placeholder="EX: EG">
                            </div>
                        </div>
                    </div>


                    <div class="citiy-head row" style="position: relative;">

                        <div class="col-md-10">

                            <h2 style="font-weight: bold;line-height: 1.5em;">Cities</h2>

                        </div>

                        <div class="col-md-2">

                            <div class="col-sm-2">
                                <button type="button" class="add_cities btn btn-primary"
                                style="position: absolute; top: 25px; right: 20px;"
                                data-url="{{route('getAddCityTemplate')}}">+</button>
                            </div>

                        </div>

                    </div>


                        <div class="row add_cities_section" style="position: relative;">
                            @foreach($country->cities as $city)
                            <div class="city_wrapper col-sm-12 row">

                                <div class="form-group col-sm-5">
                                    <label for="city" class="col-2 col-form-label ">City name in EN</label>
                                    <div class="col-10">
                                        <input class="form-control required" type="text"  name="city_name_en[]" value="{{$city->en->city}}"
                                        data-cityid={{$city->id}}
                                               placeholder="EX:Tanta">
                                    </div>
                                </div>
                                <div class="form-group col-sm-5">
                                    <label for="city" class="col-2 col-form-label" style="float: right;">اسم المدينة باللغة العربيه</label>
                                    <div class="col-10">
                                        <input class="form-control required" type="text"  name="city_name_ar[]" value="{{$city->ar ? $city->ar->city : ''}}"
                                        data-cityid={{$city->id}}
                                               placeholder='مثلا: طنطا'>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" class="remove_city btn btn-danger"
                                    style="position: absolute; top: 25px;"
                                    data-url="{{route('delete-city', ['id' => $city->id])}}"
                                    data-old="y">X</button>
                                </div>

                            </div>
                            @endforeach

                            <!-- <div class="city_wrapper col-sm-12 row">

                                <div class="form-group col-sm-5">
                                    <label for="city" class="col-2 col-form-label ">City name in EN</label>
                                    <div class="col-10">
                                        <input class="form-control required" type="text"  name="city_name_en[]"
                                               placeholder="EX:Tanta">
                                    </div>
                                </div>
                                <div class="form-group col-sm-5">
                                    <label for="city" class="col-2 col-form-label" style="float: right;">اسم المدينة باللغة العربيه</label>
                                    <div class="col-10">
                                        <input class="form-control required" type="text"  name="city_name_ar[]"
                                               placeholder="مثلا:طنطا ">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" class="remove_city btn btn-danger"
                                    style="position: absolute; top: 25px;"
                                    data-old="n">X</button>
                                </div>

                            </div> -->

                        </div>



                    <div class="modal-footer" style="text-align: center;">
                        <button type="button" class="btn-update-country-submit btn btn-primary btn-sm btn-flat"
                        style="font-weight: bold; font-size: 16px; border-radius: 5px;">
                            Save <span class="glyphicon glyphicon-save"> </span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

@section('scripts')

<script src="{{asset('resources/views/admin/pages/addresses/scripts/addresses.js')}}"></script>

@endsection
