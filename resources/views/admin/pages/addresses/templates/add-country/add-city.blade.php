<div class="city_wrapper col-sm-12 row">

    <div class="form-group col-sm-5">
        <label for="city" class="col-2 col-form-label ">City name in EN</label>
        <div class="col-10">
            <input class="form-control required" type="text" name="city_name_en[]"
                   placeholder="EX:Tanta">
        </div>
    </div>
    <div class="form-group col-sm-5">
        <label for="city" class="col-2 col-form-label"style="float: right;">اسم المدينة باللغة العربيه</label>
        <div class="col-10">
            <input class="form-control required" type="text" name="city_name_ar[]"
                   placeholder='مثلا: طنطا' style="text-align: right">
        </div>
    </div>
    <div class="col-sm-2">
        <button type="button" class="remove_city btn btn-danger"
        style="position: absolute; top: 25px;">-</button>
    </div>

</div>
