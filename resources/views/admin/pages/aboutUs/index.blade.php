@extends('admin.master')

@section('content-header')
    <section class="content-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><h4>About Us</h4></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.home')  }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.abouts.getIndex')  }}">About Us</a></li>
            </ol>
        </nav>
    </section>
@endsection

@section('content')
    <section class="content">

        <div class="panel panel-primary">
            <!-- Default panel contents -->
            <div class="panel-heading" style="font-size: large">About Us</div>

            <form action="{{ route('admin.abouts.getIndex') }}" onsubmit="return false;">
                {{ csrf_field() }}
                <div class="panel-body">
                    <a href="{{route('admin.abouts.getCreateNewAbout')}}" class="btn btn-primary btn-md">
                        <li class="fa fa-plus"> Add About</li>
                    </a>
                </div>

                <div class="table-responsive">
                    <!-- Table -->
                    <table class="table">
                        <thead>
                        <tr style="color: black; font-size: medium;">
                            <th class="text-center">Title</th>
                            <th class="text-center">Content</th>
                            <th class="text-center">Call_title</th>
                            <th class="text-center">Call_icon</th>
                            <th class="text-center">Call_value</th>
                            <th class="text-center">image</th>
                        </tr>
                        </thead>
                        <tbody class="table-hover">

                            <tr>
                                <td class="text-center"></td>
                                <td class="text-center"></td>
                                <td class="text-center"></td>
                                <td class="text-center"></td>
                                <td class="text-center"></td>
                                <td class="text-center"></td>
                                <td class="text-center">
                                    <a href="" class="btn btn-warning btn-sm">
                                        <li class="fa fa-pencil"> Edit</li>
                                    </a>

                                    <input type="hidden" name="_method" value="delete"/>

                                    <a class="btn btn-danger btn-sm" title="Delete" data-toggle="modal"
                                       href="#delete"
                                       data-id=""
                                       data-token="{{csrf_token()}}">
                                        Delete
                                    </a>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </section>

@endsection

@section('modals')
    @include('admin.pages.aboutUs.modals.delete-about')
@endsection
