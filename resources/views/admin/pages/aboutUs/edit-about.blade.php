@extends('admin.master')

@section('content-header')
    <section class="content-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><h4>Boughts</h4></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.abouts.getIndex')}}">About Us</a></li>
                <li class="breadcrumb-item"><a href="">Edit About Us</a>
                </li>
            </ol>
        </nav>
    </section>
@endsection

@section('content')

<section class="content">
    <div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading" style="font-size: large">Edit About Us</div>

        <form action="" onsubmit="return false;" method="post">
            {!! csrf_field() !!}


                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn-submit btn btn-primary btn-sm btn-flat">
                        Save <span class="glyphicon glyphicon-save"> </span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>


@endsection
