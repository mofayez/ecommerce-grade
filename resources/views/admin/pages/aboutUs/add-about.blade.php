@extends('admin.master')

@section('content-header')
    <section class="content-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><h4>About</h4></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.abouts.getIndex')}}">About Us</a></li>
                <li class="breadcrumb-item"><a href="">Add About Us</a></li>
            </ol>
        </nav>
    </section>
@endsection

@section('content')

    <section class="content">
        <div class="panel panel-primary">
            <!-- Default panel contents -->
            <div class="panel-heading" style="font-size: large">Add About Us</div>

            <form action="{{route('admin.abouts.getCreateNewAbout')}}"  class="add-form" enctype="multipart/form-data"
                  method="post"
                  onsubmit="return false;">

                {!! csrf_field() !!}

                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="keyword_en" class="col-2 col-form-label ">keyword EN</label>
                            <div class="col-10">
                                <input class="form-control required" type="text" name="keyword_en"
                                       placeholder="EX: keyword">
                            </div>
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="keyword_ar" class="col-2 col-form-label" style="float: right;">الكلمه الداله باللغه العربيه</label>
                            <div class="col-10">
                                <input class="form-control required" type="text" name="keyword_en"
                                style="direction: rtl;"
                                       placeholder="الكلمه الداله ">
                            </div>
                        </div>

                        <div class="form-group col-sm-12">
                            <label for="title_en" class="col-2 col-form-label ">Title EN</label>
                            <div class="col-10">
                                <input class="form-control required" type="text" name="title_en"
                                style="direction: rtl;"
                                       placeholder="Title in english">
                            </div>
                        </div>

                        <div class="form-group col-sm-12">
                          <label for="title_ar" class="col-2 col-form-label ">العنوان باللغه العربيه</label>
                          <div class="col-10">
                            <input class="form-control required" type="text" name="title_ar"
                            style="direction: rtl;"
                            placeholder="العنوان باللغه العربيه">
                          </div>
                        </div>

                        <div class="form-group col-sm-12">
                            <label for="content_en" class="col-2 col-form-label" style="float: right;">المحتوي باللغه العربيه </label>
                            <div class="col-10">
                                    <textarea class="form-control" rows="3" name="content_en"
                                    style="direction: rtl;"></textarea>
                            </div>
                        </div>

                        <div class="form-group col-sm-12">
                          <label for="content_ar" class="col-2 col-form-label" style="float: right;">المحتوى باللغه العربيه</label>
                          <div class="col-10">
                            <textarea class="form-control" rows="3" name="content_ar"
                            style="direction: rtl;"></textarea>
                          </div>
                        </div>

                        <div class="form-group col-sm-12">
                          <label for="image" class="col-2 col-form-label" style="float: right;">Image</label>
                          <img src=""></img>
                        </div>

                        <div class="form-group col-sm-6">
                          <label for="call_title_en" class="col-2 col-form-label ">Call Title EN</label>
                          <div class="col-10">
                              <input class="form-control required" type="text" name="call_title_en"
                              style="direction: rtl;"
                                     placeholder="Call Title in english">
                          </div>
                        </div>

                        <div class="form-group col-sm-6">
                          <label for="call_title_ar" class="col-2 col-form-label" style="float: right;">عنوان المتصل</label>
                          <div class="col-10">
                              <input class="form-control required" type="text" name="call_title_ar"
                              style="direction: rtl;"
                                     placeholder="Title in english">
                          </div>
                        </div>

                        <div class="form-group col-sm-6">
                          <label for="call_value_en" class="col-2 col-form-label ">Call Value EN</label>
                          <div class="col-10">
                              <input class="form-control required" type="text" name="call_value_en"
                              style="direction: rtl;"
                                     placeholder="Call Value in english">
                          </div>
                        </div>

                        <div class="form-group col-sm-6">
                          <label for="call_value_ar" class="col-2 col-form-label" style="float: right;"> قيمه المتصل</label>
                          <div class="col-10">
                              <input class="form-control required" type="text" name="call_value_ar"
                              style="direction: rtl;"
                                     placeholder="قيمه المتصل">
                          </div>
                        </div>

                        <div class="form-group col-sm-6">
                          <label for="call_icon_en" class="col-2 col-form-label ">Call Icon EN</label>
                          <div class="col-10">
                              <input class="form-control required" type="text" name="call_icon_en"
                              style="direction: rtl;"
                                     placeholder="Call Icon in english">
                          </div>
                        </div>

                        <div class="form-group col-sm-6">
                          <label for="call_icon_ar" class="col-2 col-form-label" style="float: right;"> ايكونه المتصل</label>
                          <div class="col-10">
                              <input class="form-control required" type="text" name="call_icon_ar"
                              style="direction: rtl;"
                                     placeholder="ايكونه المتصل">
                          </div>
                        </div>
                    </div>

                    <!-- footer div -->
                    <div class="modal-footer">
                        <button type="submit" class="btn-add-submit btn btn-primary btn-md btn-flat">
                            Save <span class="glyphicon glyphicon-save"></span>
                        </button>
                    </div>
                    <!-- end footer div -->
                </div>
                <!-- end div panel-body -->
            </form>
            <!-- end form -->
        </div>
        <!-- end panel div -->
    </section>
    <!-- end section -->
@endsection
