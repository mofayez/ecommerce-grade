@extends('admin.master')

@section('content-header')
    <section class="content-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><h4>Boughts</h4></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.faqs.getIndex')}}">FAQs</a></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.faqs.getUpdateFaqs', ['id' => $faqs->id])}}">Edit FAQs</a>
                </li>
            </ol>
        </nav>
    </section>
@endsection

@section('content')

<section class="content">
    <div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading" style="font-size: large">Edit FAQs</div>

        <form action="{{route('admin.faqs.updateFaqs',['id' => $faqs->id])}}" class="update-form" onsubmit="return false;" method="post">
            {!! csrf_field() !!}

            <div class="modal-body">

                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="bought_type" class="col-2 col-form-label">Stauts</label>
                    <div class="col-10">
                      @if($faqs->active == "1")
                      <label class="radio-inline"><input class="active" type="radio" name="status" value="1" checked>Active</label>
                      <label class="radio-inline "><input class="not_active" type="radio" name="status" value="0">Not Active</label>
                      @elseif($faqs->active == "0")
                      <label class="radio-inline"><input class="active" type="radio" name="status" value="1" >Active</label>
                      <label class="radio-inline "><input class="not_active" type="radio" name="status" value="0" checked>Not Active</label>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="question_en" class="col-2 col-form-label ">Question EN</label>
                    <div class="col-10">
                      <input class="form-control required" type="text" name="question_en" value="{{$faqs->en->question}}"
                      placeholder="EX: what about this site?">
                    </div>
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="question_ar" class="col-2 col-form-label" style="float: right;">السؤال باللغه العربيه</label>
                    <div class="col-10">
                      <input class="form-control required" type="text" name="question_ar" value="{{$faqs->ar->question}}"
                      style="direction: rtl;"
                      placeholder="ما رايك ف الموقع ؟">
                    </div>
                  </div>

                  <div class="form-group col-sm-12">
                    <label for="answer_en" class="col-2 col-form-label ">Answer EN</label>
                    <div class="col-10">
                      <textarea class="form-control" rows="3" id="answer_en" name="answer_en">{{$faqs->en->answer}}</textarea>
                    </div>
                  </div>

                  <div class="form-group col-sm-12">
                    <label for="answer_ar" class="col-2 col-form-label" style="float: right;">الاجابه باللغه العربيه </label>
                    <div class="col-10">
                      <textarea class="form-control" rows="3" name="answer_ar"
                      style="direction: rtl;">{{$faqs->ar->answer}}</textarea>
                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn-update-submit btn btn-primary btn-sm btn-flat">
                        Save <span class="glyphicon glyphicon-save"> </span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>

@endsection
