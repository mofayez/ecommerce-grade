@extends('admin.master')

@section('content-header')
    <section class="content-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><h4>FAQs</h4></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.faqs.getIndex')}}">FAQs</a></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.faqs.getCreateNewFaqs')  }}">Add FAQs</a></li>
            </ol>
        </nav>
    </section>
@endsection

@section('content')

    <section class="content">
        <div class="panel panel-primary">
            <!-- Default panel contents -->
            <div class="panel-heading" style="font-size: large">Add FAQs</div>

            <form action="{{route('admin.faqs.createNewFaqs')}}"  class="add-form" enctype="multipart/form-data"
                  method="post"
                  onsubmit="return false;">

                {!! csrf_field() !!}

                <div class="modal-body">

                    <div class="row">
                      <div class="form-group col-sm-6">
                          <label for="status" class="col-2 col-form-label">Stauts</label>
                          <div class="col-10">
                              <label class="radio-inline"><input class="active" type="radio" name="status" value="1" checked>Active</label>
                              <label class="radio-inline "><input class="not_active" type="radio" name="status" value="0">Not Active</label>
                          </div>
                      </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="question_en" class="col-2 col-form-label ">Question EN</label>
                            <div class="col-10">
                                <input class="form-control required" type="text" name="question_en"
                                       placeholder="EX: what about this site?">
                            </div>
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="question_ar" class="col-2 col-form-label" style="float: right;">السؤال باللغه العربيه</label>
                            <div class="col-10">
                                <input class="form-control required" type="text" name="question_ar"
                                style="direction: rtl;"
                                       placeholder="ما رايك ف الموقع ؟">
                            </div>
                        </div>

                        <div class="form-group col-sm-12">
                            <label for="answer_en" class="col-2 col-form-label ">Answer EN</label>
                            <div class="col-10">
                                    <textarea class="form-control" rows="3" id="answer_en" name="answer_en"></textarea>
                            </div>
                        </div>

                        <div class="form-group col-sm-12">
                            <label for="answer_ar" class="col-2 col-form-label" style="float: right;">الاجابه باللغه العربيه </label>
                            <div class="col-10">
                                    <textarea class="form-control" rows="3" name="answer_ar"
                                    style="direction: rtl;"></textarea>
                            </div>
                        </div>
                    </div>

                    <!-- footer div -->
                    <div class="modal-footer">
                        <button type="submit" class="btn-add-submit btn btn-primary btn-md btn-flat">
                            Save <span class="glyphicon glyphicon-save"></span>
                        </button>
                    </div>
                    <!-- end footer div -->
                </div>
                <!-- end div panel-body -->
            </form>
            <!-- end form -->
        </div>
        <!-- end panel div -->
    </section>
    <!-- end section -->
@endsection
