@extends('admin.master')

@section('content-header')
    <section class="content-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><h4>FAQs</h4></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.home')  }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.faqs.getIndex')  }}">FAQs</a></li>
            </ol>
        </nav>
    </section>
@endsection

@section('content')
    <section class="content">

        <div class="panel panel-primary">
            <!-- Default panel contents -->
            <div class="panel-heading" style="font-size: large">FAQs</div>

            <form action="{{ route('admin.faqs.getIndex') }}" onsubmit="return false;">
                {{ csrf_field() }}
                <div class="panel-body">
                    <a href="{{route('admin.faqs.getCreateNewFaqs')}}" class="btn btn-primary btn-md">
                        <li class="fa fa-plus"> Add FAQs</li>
                    </a>
                </div>

                <div class="table-responsive">
                    <!-- Table -->
                    <table class="table">
                        <thead>
                        <tr style="color: black; font-size: medium;">
                            <th class="text-center">Question</th>
                            <th class="text-center">Answer</th>
                            <th class="text-center">Active</th>
                        </tr>
                        </thead>
                        <tbody class="table-hover">
                          @foreach($faqs as $faq)
                            <tr>
                                <td class="text-center">{{$faq->faq_translated->question}}</td>
                                <td class="text-center">{{$faq->faq_translated->answer}}</td>
                                <td class="text-center">{{$faq->active}}</td>
                                <td class="text-center">
                                    <a href="{{ route('admin.faqs.getUpdateFaqs', ['id' => $faq->id])}}" class="btn btn-warning btn-sm">
                                        <li class="fa fa-pencil"> Edit</li>
                                    </a>

                                    <input type="hidden" name="_method" value="delete"/>

                                    <a class="btn btn-danger btn-sm" title="Delete" data-toggle="modal"
                                       href="#delete"
                                       data-id="{{$faq->id}}"
                                       data-token="{{csrf_token()}}">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </section>

@endsection


@section('modals')
    @include('admin.pages.FAQs.modals.delete-faqs')
@endsection
