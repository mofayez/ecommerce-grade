@extends('admin.master')

@section('content-header')
    <section class="content-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><h4>Boughts</h4></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.settings.getIndex')}}">Settings</a></li>
                <li class="breadcrumb-item"><a href="">Edit Settings</a>
                </li>
            </ol>
        </nav>
    </section>
@endsection

@section('content')

<section class="content">
    <div class="panel panel-primary">
        <!-- Default panel contents -->
        <div class="panel-heading" style="font-size: large">Roles</div>

        <form action="{{ route('admin.settings.updateSettings', ['id' => $settings->id])}}" onsubmit="return false;" method="post"
          class="update-form">

                {!! csrf_field() !!}
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-offset-5 col-md-2">
                        <div class="box box-primary">
                            <img style="cursor:pointer;"
                                 class="logo-settings-img file-btn img-responsive img-circle"
                                 src="{{url('storage/uploads/images/avatars/admin-avatar-default.png')}}"
                                 alt="Logo settings picture"
                                 name="logo">
                        </div>
                    </div>
                </div>

                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="name" class="col-2 col-form-label ">Name</label>
                    <div class="col-10">
                      <input class="form-control required" type="text" id="name" name="name"
                      placeholder="EX: mahmoud" value="{{$settings->name}}">
                    </div>
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="email" class="col-2 col-form-label">Email</label>
                      <div class="col-10">
                        <input class="form-control required" type="text" id="email" name="email" placeholder="mahmoud@gmail.com"
                        value="{{$settings->email}}">
                      </div>
                  </div>
                </div>


                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="phone" class="col-2 col-form-label">Phone</label>
                      <div class="col-10">
                        <input class="form-control required" type="text" id="phone" name="phone" placeholder="phone"
                        value="{{$settings->phone}}">
                      </div>
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="mobile" class="col-2 col-form-label">Mobile</label>
                      <div class="col-10">
                        <input class="form-control required" type="text" id="mobile" name="mobile" placeholder="01227678387"
                        value="{{$settings->mobile}}">
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="form-group col-sm-6">
                      <label for="gender" class="col-2 col-form-label">Gender</label>
                      <div class="col-10">
                          <select class="form-control" name="gender" id="gender">
                              <option value="">{{$settings->gender}}</option>
                              <option value="male">male</option>
                              <option value="female">female</option>
                          </select>
                      </div>
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="address_1" class="col-2 col-form-label">Address-1</label>
                    <div class="col-10">
                      <input class="form-control required" type="text" id="address_1" name="address_1" placeholder="cairo"
                      value="{{$settings->address_1}}">
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="form-group col-sm-6">
                    <label for="address_2" class="col-2 col-form-label">Address-2</label>
                    <div class="col-10">
                      <input class="form-control required" type="text" id="address_2" name="address_2" placeholder="tanta"
                      value="{{$settings->address_2}}">
                    </div>
                  </div>

                  <div class="form-group col-sm-6">
                    <label for="title" class="col-2 col-form-label">Title</label>
                    <div class="col-10">
                      <input class="form-control required" type="text" id="title" name="title" placeholder="title"
                      value="{{$settings->title}}">
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="form-group col-sm-12">
                      <label for="about" class="col-2 col-form-label">About</label>
                      <div class="col-10">
                          <div class="col-10">
                              <textarea class="form-control required" rows="5" id="about" name="about"
                                        placeholder="about site">{{$settings->about}}</textarea>
                          </div>
                      </div>
                  </div>

                  <div class="form-group col-sm-12">
                      <label for="meta_description" class="col-2 col-form-label">Meta Description</label>
                      <div class="col-10">
                          <div class="col-10">
                              <textarea class="form-control required" rows="5" id="meta_description" name="meta_description"
                                        placeholder="meta description about site!">{{$settings->meta_descriptions}}</textarea>
                          </div>
                      </div>
                  </div>
                </div>

                 <div class="modal-footer">
                    <button type="submit" class="btn-update-submit btn btn-warning btn-md btn-flat">
                        Edit <span class="glyphicon glyphicon-save"> </span>
                    </button>
                 </div>

            </div>
        </form>
    </div>
</section>

@endsection
