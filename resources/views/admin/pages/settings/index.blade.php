@extends('admin.master')

@section('content-header')
    <section class="content-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><h4>Seetings</h4></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.home')  }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.settings.getIndex')  }}">Settings</a></li>
            </ol>
        </nav>
    </section>
@endsection

@section('content')
    <section class="content">

        <div class="panel panel-primary">
            <!-- Default panel contents -->
            <div class="panel-heading" style="font-size: large">Roles</div>

            <form action="{{ route('admin.settings.getIndex') }}" onsubmit="return false;">
                {{ csrf_field() }}
                <div class="panel-body">
                    <a href="{{route('admin.settings.getCreateNewSettings')}}" class="btn btn-primary btn-md">
                        <li class="fa fa-plus"> Add Settings</li>
                    </a>
                </div>

                <div class="table-responsive">
                    <!-- Table -->
                    <table class="table">
                        <thead>
                        <tr style="color: black; font-size: medium;">
                            <th class="text-center">Name</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Genger</th>
                            <th class="text-center">Phone</th>
                            <th class="text-center">address-1</th>
                            <th class="text-center">title</th>
                            <th class="text-center">about</th>
                            <th class="text-center">logo</th>
                        </tr>
                        </thead>
                        <tbody class="table-hover">
                          @foreach($settings as $setting)
                            <tr>
                                <td class="text-center">{{ $setting->name }}</td>
                                <td class="text-center">{{ $setting->email }}</td>
                                <td class="text-center">{{ $setting->gender }}</td>
                                <td class="text-center">{{ $setting->phone }}</td>
                                <td class="text-center">{{ $setting->address_1 }}</td>
                                <td class="text-center">{{ $setting->title }}</td>
                                <td class="text-center">{{ $setting->about }}</td>
                                <td class="text-center">{{ $setting->logo }}</td>
                                <td class="text-center">
                                    <a href="{{route('admin.settings.getUpdateSettings', ['id' => $setting->id])}}" class="btn btn-warning btn-sm">
                                        <li class="fa fa-pencil"> Edit</li>
                                    </a>

                                    <input type="hidden" name="_method" value="delete"/>

                                    <a class="btn btn-danger btn-sm" title="Delete" data-toggle="modal"
                                       href="#delete"
                                       data-id="{{$setting->id}}"
                                       data-token="{{csrf_token()}}">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </section>

@endsection

@section('modals')
    @include('admin.pages.settings.modals.delete-settings')
@endsection
