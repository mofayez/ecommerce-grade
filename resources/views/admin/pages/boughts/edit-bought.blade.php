@extends('admin.master')

@section('content-header')
    <section class="content-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><h4>Boughts</h4></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.boughts.getIndex')}}">Boughts</a></li>
                <li class="breadcrumb-item"><a href="{{  route('admin.boughts.getUpdateBought', ['id' => $main_bought->id])}}">Edit Boughts</a>
                </li>
            </ol>
        </nav>
    </section>
@endsection

@section('content')

  <section class="content">
    <div class="panel panel-primary">
      <!-- Default panel contents -->
      <div class="panel-heading" style="font-size: large">Boughts</div>

        <form action="{{ route('admin.boughts.updateBought', ['id' => $main_bought->id]) }}" onsubmit="return false;" method="post">

          {!! csrf_field() !!}

            <div class="panel-body">
                <div class="mainBought">
                  <!-- first div row -->
                  <div class="row">
                      <!-- users div  -->
                      <div class="form-group col-sm-6">
                          <label for="role" class="col-2 col-form-label">User`s Vendor</label>
                          <div class="col-10">
                              <select class="form-control" name="role_id" class="role">
                                  <option value="{{$main_bought->user_id}}">{{$user_name}}</option>
                                  @foreach($users as $user)
                                    <option value="{{$user['id']}}">{{$user['name']}}</option>
                                  @endforeach
                              </select>
                          </div>
                      </div>
                      <!-- end users div -->

                      <!-- bought type div  -->
                      <div class="form-group col-sm-6">
                          <label for="bought_type" class="col-2 col-form-label">Bought Type</label>
                          <div class="col-10">
                              @if($main_bought->type == 'cache')
                                <label class="radio-inline"><input class="cache" type="radio" name="bought_type" value="cache" onchange="return Cache();" checked>Cache</label>
                                <label class="radio-inline "><input class="remainder" type="radio" name="bought_type" value="remainder"  onchange="return Remainder();">Remainder</label>
                              @elseif($main_bought->type == 'remainder')
                                <label class="radio-inline"><input class="cache" type="radio" name="bought_type" value="cache" onchange="return Cache();">Cache</label>
                                <label class="radio-inline "><input class="remainder" type="radio" name="bought_type" value="remainder"  onchange="return Remainder();"  checked>Remainder</label>
                              @endif
                          </div>
                      </div>
                      <!-- end bought type div  -->
                  </div>
                  <!-- end first div row  -->

                  <!-- secound div row -->
                  <div class="row">
                      <!-- paid div  -->
                      <div class="form-group col-sm-6">
                          <label for="paid" class="col-2 col-form-label ">Paid</label>
                          <div class="col-10">
                              <input class="form-control" type="text" class="paid" name="paid"
                                     placeholder="paid" value="{{$main_bought->paid}}">
                          </div>
                      </div>
                      <!-- end paid div -->

                      <!-- remain div  -->
                      <div class="form-group col-sm-6" >
                          <label for="remain" class="col-2 col-form-label">Remain</label>
                          <div class="col-10">
                              <input class="form-control" type="text" class="remain"
                                     name="remain"
                                     placeholder="remain" value="{{$main_bought->remain}}">
                          </div>
                      </div>
                      <!-- end remain div -->
                  </div>
                  <!-- end secound div row  -->
                </div>

                <hr style="border: 0; height: 3px; background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));">


                <!-- div plus button -->
                <div class="form-group col-sm-2" style="margin-top: 45px; margin-left: 1000px;">
                    <div class="col-10">
                        <button type="button" name="update_product_section"
                                data-url = "{{route('admin.boughts.getUpdateBoughtSectionView')}}"
                                class="btn btn-primary btn-md update_product_section">+
                        </button>
                    </div>
                </div>
                <!-- end div plus button  -->

                <div class="Boughts" >
                  @foreach($bought_details as $bought)
                  <!-- boughtsWrapper div -->
                  <div class="boughtsWrapper row">
                      <!-- bought div  -->
                      <div class="bought col-sm-12 row" style="margin-bottom: 35px;">

                          <input name="boughtId" type="hidden" value="
                            @if($bought->product_type == 'option')
                              {{$bought->product_option_value_id}}
                            @elseif($bought->product_type == 'normal')
                              {{$bought->NormalProductDetails->id}}
                            @endif"/>

                          <!-- product div -->
                          <div class="row col-sm-12">
                              <div class="form-group col-sm-4">
                                  <label style="font-size: 30px; font-family: 'Source Sans Pro',sans-serif; font-weight: 500; color: #FF4136;">product</label>
                                  <select class="form-control selectpicker" data-live-search="true" name="products[]" onchange="handleProductChange(event)">
                                      <option data-type="{{$bought->product_type}}" value="{{$bought->product_id}}">{{$bought->productName}}</option>
                                      <!-- @foreach($products as $product )
                                          <option data-type="{{$product->type}}" value="{{$product->id}}">{{$product->trans->name}}</option>
                                      @endforeach -->
                                  </select>
                              </div>
                              <!-- div remove button -->
                              <div class="form-group col-sm-2" style="margin-top: 45px;">
                                  <div class="col-10">
                                    <button type="button" name="remove"
                                      class="btn btn-danger btn-md product-remove"
                                      data-url="{{route('admin.boughts.deleteProductBought', ['id' => $bought->id])}}">X</button>
                                  </div>
                              </div>
                              <!-- end div remove button  -->

                          </div>
                          <!-- end product div -->

                          <!-- productNormalWrapper div -->
                          <div class="productNormalWrapper
                          {{$bought->product_type == 'option'? 'hidden' : ''}}">
                              <!-- productNormal div  -->
                              <div class="productNormal col-sm-12">

                                <input name="bought_details_id" type="hidden" value="{{$bought->id}}"/>

                                  <!-- price div  -->
                                  <div class="form-group col-sm-4">
                                      <label for="price" class="col-2 col-form-label ">Price</label>
                                      <div class="col-10">
                                          <input class="form-control" type="text" class="price"
                                                 name="price" value="{{$bought->NormalProductDetails['price']}}"
                                                 placeholder="price">
                                      </div>
                                  </div>
                                  <!-- end price div -->

                                  <!-- serial div  -->
                                  <div class="form-group col-sm-4">
                                      <label for="serial" class="col-2 col-form-label ">Serial</label>
                                      <div class="col-10">
                                          <input class="form-control" type="text" class="serial"
                                                 name="serial" value="{{$bought->NormalProductDetails['serial']}}"
                                                 placeholder="serial">
                                      </div>
                                  </div>
                                  <!-- end serial div -->

                                  <!-- model number div -->
                                  <div class="form-group col-sm-4">
                                      <label for="model_number" class="col-2 col-form-label ">Model Number</label>
                                      <div class="col-10">
                                          <input class="form-control" type="text" class="model_number"
                                                 name="model_number" value="{{$bought->NormalProductDetails['model_number']}}"
                                                 placeholder="model number">
                                      </div>
                                  </div>
                                  <!-- end model number div -->

                                  <!-- barcode div -->
                                  <div class="form-group col-sm-4">
                                      <label for="barcode" class="col-2 col-form-label ">Barcode</label>
                                      <div class="col-10">
                                          <input class="form-control" type="text" class="barcode"
                                                 name="barcode" value="{{$bought->NormalProductDetails['barcode']}}"
                                                 placeholder="barcode">
                                      </div>
                                  </div>
                                  <!-- end barcode div -->

                                  <!-- discount div -->
                                  <div class="form-group col-sm-4">
                                      <label for="discount" class="col-2 col-form-label ">Discount</label>
                                      <div class="col-10">
                                          <input class="form-control" type="text" class="discount"
                                                 name="discount" value="{{$bought->NormalProductDetails['discount']}}"
                                                 placeholder="discount">
                                      </div>
                                  </div>
                                  <!-- end discount div -->

                                  <!-- discount type div -->
                                  <div class="form-group col-sm-4">
                                      <label for="discount_type" class="col-2 col-form-label ">Discount Type</label>
                                      <div class="col-10">
                                        <select class="form-control" name="discount_type">
                                          <option value="{{$bought->NormalProductDetails['discount_type']}}">{{$bought->NormalProductDetails['discount_type']}}</option>
                                          <option value="percent">Prcent</option>
                                          <option value="amount">Amount</option>
                                        </select>
                                      </div>
                                  </div>
                                  <!-- end discount type div -->
                                  <!-- amount div -->
                                  <div class="form-group col-sm-6">
                                      <label for="amount" class="col-2 col-form-label ">Amount</label>
                                      <div class="col-10">
                                          <input class="form-control" type="text" class="amount"
                                                 name="amount" value="{{$bought->amount}}"
                                                 placeholder="20">
                                      </div>
                                  </div>
                                  <!-- end amount div -->

                                  <!-- cost div -->
                                  <div class="form-group col-sm-6">
                                      <label for="cost" class="col-2 col-form-label ">cost</label>
                                      <div class="col-10">
                                          <input class="form-control" type="text" class="cost"
                                                 name="cost" value="{{$bought->cost}}"
                                                 placeholder="cost">
                                      </div>
                                  </div>
                                  <!-- end cost div -->
                              </div>
                              <!-- end class productNormal div  -->
                          </div>
                          <!-- end class productNormalWrapper div -->

                          <!-- ProductOptionWrapper div -->
                          <div class="productOptionWrapper
                          {{$bought->product_type == 'normal'? 'hidden' : ''}}">

                              <input name="bought_details_id" type="hidden" value="{{$bought->id}}"/>

                              <!-- ProductOption div -->
                              <div class="productOption col-sm-12">
                                  <h2>options</h2>
                                  @foreach($bought->option_value_ids as $opt)
                                  <!-- addOptionWrapper div row -->
                                  <div class="updateOptionWrapper row" style="margin-bottom: 30px;">
                                      <!-- addOption div -->
                                      <div class="updateOption col-sm-12">

                                          <!-- options div -->
                                          <div class="form-group col-sm-4">
                                              <label for="options" class="col-2 col-form-label">option</label>
                                              <div class="col-10">
                                                  <select class="form-control" name="options[]" class="options"
                                                  onchange="updateHandleOptionChange(event)">
                                                        <option data-url="{{route('admin.boughts.getUpdateOptionValues', ['id' => $opt->option_id])}}"
                                                        value="{{$opt->option_id}}">{{$opt->option_name}}</option>
                                                      @foreach($options as $option)
                                                          <option data-url="{{route('admin.boughts.getOptionValues', ['id' => $option->id])}}"
                                                          value="{{$option->id}}">{{$option->trans->option}}</option>
                                                      @endforeach
                                                  </select>
                                              </div>
                                          </div>
                                          <!-- end options div -->

                                          <!--  option values div -->
                                          <div class="form-group col-sm-3">
                                              <label for="option_values" class="col-2 col-form-label">option values</label>
                                              <div class="col-10">
                                                  <select class="selectpicker" name="option_values[]" class="option_values">

                                                      <option value="{{$opt->option_value_id}}" selected>{{$opt->option_value}}</option>

                                                      @foreach($options->first()->values as $value)

                                                          <option value="{{$value->id}}">{{$value->trans->value}}</option>

                                                      @endforeach

                                                  </select>
                                              </div>
                                          </div>
                                          <!-- end option values div -->

                                          <!-- add button for new option section div  -->
                                          <div class="form-group col-sm-2">
                                              <label for="" class="col-2 col-form-label"></label>
                                              <div class="col-10">
                                                  <button type="button" name="add_option_section"
                                                  data-url="{{route('admin.boughts.getOptionSectionView')}}"
                                                          class="btn btn-primary btn-md add_option_section">+
                                                  </button>
                                              </div>
                                          </div>
                                          <!-- end add button div -->
                                      </div>
                                      <!-- end class addOption div  -->
                                  </div>
                                  <!-- end class addOptionWrapper div -->
                                  @endforeach
                                  <!-- serial div -->
                                  <div class="form-group col-sm-4">
                                      <label for="serial" class="col-2 col-form-label ">Serial</label>
                                      <div class="col-10">
                                          <input class="form-control" type="text" class="serial"
                                                 name="serial" value="{{$bought->ProductOptionValue['serial']}}"
                                                 placeholder="serial">
                                      </div>
                                  </div>
                                  <!-- end serial div -->

                                  <!-- model number div  -->
                                  <div class="form-group col-sm-4">
                                      <label for="model_number" class="col-2 col-form-label ">Model Number</label>
                                      <div class="col-10">
                                          <input class="form-control" type="text" class="model_number"
                                                 name="model_number" value="{{$bought->ProductOptionValue['model_number']}}"
                                                 placeholder="model number">
                                      </div>
                                  </div>
                                  <!-- end model number div -->

                                  <!-- barcode div -->
                                  <div class="form-group col-sm-4">
                                      <label for="barcode" class="col-2 col-form-label ">Barcode</label>
                                      <div class="col-10">
                                          <input class="form-control" type="text" class="barcode"
                                                 name="barcode" value="{{$bought->ProductOptionValue['barcode']}}"
                                                 placeholder="barcode">
                                      </div>
                                  </div>
                                  <!-- end barcode div -->

                                  <!-- discount div -->
                                  <div class="form-group col-sm-6">
                                      <label for="discount" class="col-2 col-form-label ">Discount</label>
                                      <div class="col-10">
                                          <input class="form-control" type="text" class="discount"
                                                 name="discount" value="{{$bought->ProductOptionValue['discount']}}"
                                                 placeholder="discount">
                                      </div>
                                  </div>
                                  <!-- end discount div -->

                                  <!-- amount div -->
                                  <div class="form-group col-sm-6">
                                      <label for="amount" class="col-2 col-form-label ">Amount</label>
                                      <div class="col-10">
                                          <input class="form-control" type="text" class="amount"
                                                 name="amount" value="{{$bought->amount}}"
                                                 placeholder="amount">
                                      </div>
                                  </div>
                                  <!-- end amount div -->

                                  <!-- cost div -->
                                  <div class="form-group col-sm-6">
                                      <label for="cost" class="col-2 col-form-label ">cost</label>
                                      <div class="col-10">
                                          <input class="form-control" type="text" class="cost"
                                                 name="cost" value="{{$bought->cost}}"
                                                 placeholder="cost">
                                      </div>
                                  </div>
                                  <!-- price div  -->

                                  <div class="form-group col-sm-6" >
                                      <label for="price" class="col-2 col-form-label">price</label>
                                      <div class="col-10">
                                          <input class="form-control" type="text" class="price"
                                                 name="price" value="{{$bought->ProductOptionValue['price']}}"
                                                 placeholder="price">
                                      </div>
                                  </div>
                                  <!-- end price div -->
                              </div>
                              <!-- end class productOption div -->
                          </div>
                          <!-- end class ProductOptionWrapper div -->

                      </div>
                      <!-- end class bought div -->
                  </div>
                  <!-- end class boughtsWrapper div -->
                  @endforeach
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn-update-submit btn btn-warning btn-md btn-flat">
                        Edit <span class="glyphicon glyphicon-save"> </span>
                    </button>
                    <!-- end footer div -->
                </div>
            </div>
              <!-- end div panel-body -->
            </form>
            <!-- end form -->
          </div>
      <!-- end panel div -->
  </section>
      <!-- end section -->
@endsection

@section('scripts')

<script src="{{asset('resources/views/admin/pages/boughts/scripts/boughts.js')}}"></script>

@endsection
