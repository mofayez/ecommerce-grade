$('.selectpicker').selectpicker();

//-------------------------------------------------------------//
     // handelProductChange function
//------------------------------------------------------------//
function handleProductChange(event) {

    // cast js element to jquery element
    var _this = $(event.target);
    var type = _this.find(':selected').data('type');

    if (type == 'normal') {

        _this.closest('.bought').find('.productNormalWrapper').removeClass('hidden');
        _this.closest('.bought').find('.productOptionWrapper').removeClass('hidden').addClass('hidden');

        console.log();
    } else {
        _this.closest('.bought').find('.productNormalWrapper').removeClass('hidden').addClass('hidden');
        _this.closest('.bought').find('.productOptionWrapper').removeClass('hidden');
    }
}
//------------------------------------------------------//
                 //end of function
//------------------------------------------------------//

//-------------------------------------------------------------//
     // handelOptionChange function
//------------------------------------------------------------//
function handleOptionChange(event) {

    var _this = $(event.target);
    var url = _this.find(':selected').data('url');

    $.ajax({
        url: url,
        method:"GET",
        success:function(result){

            _this.closest('.addOption').find('select').last().empty().html(result).selectpicker('refresh');

        }
    });
}
//------------------------------------------------------//
                 //end of function
//------------------------------------------------------//

//-------------------------------------------------------------//
     // updateHandelOptionChange function
//------------------------------------------------------------//
function updateHandleOptionChange(event) {

    var _this = $(event.target);
    var url = _this.find(':selected').data('url');

    $.ajax({
        url: url,
        method:"GET",
        success:function(result){

            _this.closest('.updateOption').find('select').last().empty().html(result).selectpicker('refresh');

        }
    });
}
//------------------------------------------------------//
                 //end of function
//------------------------------------------------------//


//-------------------------------------------------------------//
     // cache and remainder radio button section in bought page
//------------------------------------------------------------//

//-------------------Cache function --------------------//
function Cache() {
    var cache_type = $("input:radio[class='cache']").is(":checked");
    var remain = $("input[name='remain']");
    if(cache_type){
        remain.prop('disabled', true);
    }
}

//-------------------Remainder function --------------------//
function Remainder() {
    var remainder_type = $("input:radio[class='remainder']").is(":checked");
    var remain = $("input[name='remain']");
    if(remainder_type){
        remain.removeAttr("disabled");
    }
}
//------------------------------------------------------//
                 //end of function
//------------------------------------------------------//


$(function() {
    var $radios = $('input:radio[name=bought_type]');
    if($radios.is(':checked') === false) {
        $radios.filter('[value=cache]').prop('checked', true);
    }
    var cache_type = $("input:radio[class='cache']").is(":checked");
    var remain = $("input[name='remain']");
    if(cache_type){
        remain.prop('disabled', true);
    }
});



//--------------------------------------------------//
     // add option section in bought page
//--------------------------------------------------//
$(document).on('click','.add_option_section',function(){
    var html='';
    $.ajax({
        url: $(this).data('url'),
        method: 'GET',
        success: function (result) {
            html += result;
            $('.addOptionWrapper').append(html);

            $('.selectpicker').selectpicker();
        }
    });
});

//-------------------Remove section --------------------//
$(document).on('click','.remove_option_section',function(){
    $(this).closest('.addOption').remove();
});
//------------------------------------------------------//
                 //end of function
//------------------------------------------------------//


//--------------------------------------------------//
     // add prodcut section in bought page
//--------------------------------------------------//
$(document).on('click','.add_product_section',function(){
    var html = '';

    $.ajax({
        url: $(this).data('url'),
        method: 'GET',
        success: function (result) {
            html += result;
            $('.boughtsWrapper').append(html);

            $('.selectpicker').selectpicker();
        }
    });
});

//-------------------Remove section --------------------//
$(document).on('click','.remove_product_form',function(){
    $(this).closest('.bought').remove();
});
//------------------------------------------------------//
                 //end of function
//------------------------------------------------------//


//--------------------------------------------------//
     // update prodcut section in bought page
//--------------------------------------------------//
$(document).on('click','.update_product_section',function(){
    var html = '';

    $.ajax({
        url: $(this).data('url'),
        method: 'GET',
        success: function (result) {
            html += result;

            $('.Boughts').append(html);

            $('.selectpicker').selectpicker();
        }
    });
});

//-------------------Remove section --------------------//
$(document).on('click','.update_remove_product_form',function(){
    $(this).closest('.bought').remove();
});
//------------------------------------------------------//
                 //end of function
//------------------------------------------------------//



//------------------------------------------------------//
                 //add bought function
//------------------------------------------------------//

$(document).on('click', '.btn-add-submit', function(e){
    e.preventDefault();

    var products = $("select[name='products[]']");

    var action_url = $(this).closest('form').attr('action');

    var data = [];
    var mainBought = [];
    var normal_products = [];
    var option_products = [];

    var mainBoughts = {
        user_id:  $("select[name='role_id']").val(),
        bought_type: $("input:radio[name='bought_type']:checked").val(),
        total_price: $("input[name='total_price']").val(),
        total_amount: $("input[name='total_amount']").val(),
        paid: $("input[name='paid']").val(),
        remain: $("input[name='remain']").val(),
    };

    mainBought.push(mainBoughts);

    products.each(function(key, item){

        type = $(this).find(':selected').data('type');

        if(type == 'normal'){

            var data_normal_object = {
                id: $(this).find(':selected').val(),
                type: $(this).find(':selected').data('type'),
                price: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='price']").val(),
                serial: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='serial']").val(),
                model_number: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='model_number']").val(),
                barcode: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='barcode']").val(),
                discount: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='discount']").val(),
                stock: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='stock']").val(),
                discount_type: $("select[name='discount_type']").val(),
                amount: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='amount']").val(),
                cost: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='cost']").val(),
            };

            normal_products.push([data_normal_object]);

        }else
        {
            var options_array = [];

            var options = $(this).closest('.bought').find('.productOptionWrapper').find('.addOption').find("select[name='options[]']");
            var option_values = $(this).closest('.bought').find('.productOptionWrapper').find('.addOption').find("select[name='option_values[]']");


            var opts = [];
            var vals = [];


            options.each(function (key, item){
                opts.push($(this).val());
            });

            option_values.each(function (key, item){
                vals.push($(this).val());
            });



            var array_of_options = {
                options: opts,
                option_values: vals,
            };

            options_array.push(array_of_options);

            var data_option_object ={
                id: $(this).find(':selected').val(),
                type: $(this).find(':selected').data('type'),
                options_array: options_array,
                price: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='price']").val(),
                serial: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='serial']").val(),
                model_number: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='model_number']").val(),
                barcode: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='barcode']").val(),
                discount: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='discount']").val(),
                stock: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='stock']").val(),
                amount: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='amount']").val(),
                cost: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='cost']").val(),
            };

            option_products.push([data_option_object]);
        }
    });

    $.ajax({
        url: action_url,
        method: 'POST',
        data:{
            mainBought: mainBought,
            normal_products: normal_products,
            option_products: option_products
        },
        success: function(data) {
            if (data.status == 'success') {
              new Noty({
                  layout   : 'topRight',
                  type     : 'success',
                  theme    : 'relax',
                  timeout  : true,
                  dismissQueue: true,
                  text     : [data.text],
              }).show()

              setTimeout(function() {
                  location.reload(0);
              },2000);

            } else {
                //error code...
                new Noty({
                    layout   : 'topRight',
                    type     : 'error',
                    theme    : 'relax',
                    timeout: 1500,
                    text: [data.text],
                }).show();
            }
        },
        error:function (data) {
          new Noty({
              layout   : 'topRight',
              type     : 'error',
              theme    : 'relax',
              timeout: 1500,
              text: [data.text],
          }).show();
        }
    });
});
//------------------------------------------------------//
                 //end of function
//------------------------------------------------------//


//------------------------------------------------------//
                 //update bought function
//------------------------------------------------------//

$(document).on('click', '.btn-update-submit', function(e){
    e.preventDefault();

    var products = $("select[name='products[]']");

    var Id = $("input[name='boughtId']").val();

    var Bought_Details_Id = $("input[name='bought_details_id']").val();

    var action_url = $(this).closest('form').attr('action');

    var data = [];
    var mainBought = [];
    var normal_products = [];
    var option_products = [];

    var mainBoughts = {
        user_id:  $("select[name='role_id']").val(),
        bought_type: $("input:radio[name='bought_type']:checked").val(),
        paid: $("input[name='paid']").val(),
        remain: $("input[name='remain']").val()
    };

    mainBought.push(mainBoughts);

    products.each(function(key, item){

        type = $(this).find(':selected').data('type');

        if(type == 'normal'){

            var data_normal_object = {
                id: $(this).find(':selected').val(),
                type: $(this).find(':selected').data('type'),
                price: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='price']").val(),
                serial: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='serial']").val(),
                model_number: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='model_number']").val(),
                barcode: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='barcode']").val(),
                discount: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='discount']").val(),
                stock: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='stock']").val(),
                discount_type: $("select[name='discount_type']").val(),
                amount: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='amount']").val(),
                cost: $(this).closest('.bought').find('.productNormalWrapper').find("input[name='cost']").val(),
            };

            normal_products.push([data_normal_object]);
        }else
        {
            var options_array = [];

            var options = $(this).closest('.bought').find('.productOptionWrapper').find('.updateOption').find("select[name='options[]']");
            var option_values = $(this).closest('.bought').find('.productOptionWrapper').find('.updateOption').find("select[name='option_values[]']");

            var opts = [];
            var vals = [];

            options.each(function (key, item){
                opts.push($(this).val());
            });

            option_values.each(function (key, item){
                vals.push($(this).val());
            });

            var array_of_options = {
                options: opts,
                option_values: vals,
            };

            options_array.push(array_of_options);

            var data_option_object ={
                id: $(this).find(':selected').val(),
                type: $(this).find(':selected').data('type'),
                options_array: options_array,
                price: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='price']").val(),
                serial: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='serial']").val(),
                model_number: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='model_number']").val(),
                barcode: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='barcode']").val(),
                discount: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='discount']").val(),
                stock: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='stock']").val(),
                amount: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='amount']").val(),
                cost: $(this).closest('.bought').find('.productOptionWrapper').find("input[name='cost']").val(),
            };

            option_products.push(data_option_object);
        }
        console.log(option_products);
    });
    $.ajax({
        url: action_url,
        method: 'POST',
        data:{
            Id: Id,
            Bought_Details_Id: Bought_Details_Id,
            mainBought: mainBought,
            normal_products: normal_products,
            option_products: option_products
        },
        success: function(data) {
            if (data.status == 'success') {
              new Noty({
                  layout   : 'topRight',
                  type     : 'success',
                  theme    : 'relax',
                  timeout  : true,
                  dismissQueue: true,
                  text     : [data.text],
              }).show()

              setTimeout(function() {
                  location.reload(0);
              },2000);

            } else {
                //error code...
                new Noty({
                    layout   : 'topRight',
                    type     : 'error',
                    theme    : 'relax',
                    timeout: 1500,
                    text: [data.text],
                }).show();
            }
        },
        error:function (data) {
          new Noty({
              layout   : 'topRight',
              type     : 'error',
              theme    : 'relax',
              timeout: 1500,
              text: [data.text],
          }).show();
        }
    });
});
//------------------------------------------------------//
                 //end of function
//------------------------------------------------------//



//------------------------------------------------------//
         //product bought remove section function
//------------------------------------------------------//
$(document).on('click', '.product-remove', function () {

  var url = $(this).data('url');

  swal({
  title: "Are you sure?",
    text: "Once deleted, you will not be able to recover this imaginary file!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  }).then((willDelete) => {
    if (willDelete) {

      $.ajax({
        url:url,
        method:'post',
        data: null,
        success: function (data) {
            if (data.status == 'success') {
              $(this).closest('.editValue').remove();
              swal("Poof! Your imaginary file has been deleted!", {
                icon: "success",
              });
                location.reload(0);
            } else {
                //error code...
                new Noty({
                    layout   : 'topRight',
                    type     : 'error',
                    theme    : 'relax',
                    timeout: 1500,
                    text: [data.text],
                }).show();
          }
        },
        error: function (data) {
          new Noty({
              layout   : 'topRight',
              type     : 'error',
              theme    : 'relax',
              timeout  : 1500,
              text     :'Internal Server Error',
          }).show();
        }
      });
    } else {}
  });
});
//------------------------------------------------------//
                 //end of function
//------------------------------------------------------//
