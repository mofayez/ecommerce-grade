@extends('admin.master')

@section('content-header')
    <section class="content-header">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><h4>boughts</h4></li>
                <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{url('boughts')}}">Boughts</a></li>
            </ol>
        </nav>
    </section>
@endsection

@section('content')
    <section class="content">

        <div class="panel panel-primary">
            <!-- Default panel contents -->
            <div class="panel-heading" style="font-size: large">Boughts</div>
            <form action="{{ url('admin/boughts/') }}" onsubmit="return false;">

                {{ csrf_field() }}

                <div class="panel-body">
                    <a href="{{route('admin.boughts.getCreateNewBought')}}" class="btn btn-primary btn-sm">
                        <li class="fa fa-plus"> Add bought</li>
                    </a>
                </div>

                <div class="table-responsive">
                    <!-- Table -->
                    <table class="table">
                        <thead>
                        <tr style="color: black; font-size: medium;">
                          <th class="text-center">Total Amount</th>
                          <th class="text-center">Total Cost</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Bought Type</th>
                            <th class="text-center">Remain</th>
                            <th class="text-center">Operations</th>
                        </tr>
                        </thead>
                        <tbody class="table-hover">
                        @foreach($boughts as $bought)
                            <tr>
                              <td class="text-center">{{ $bought->total_amount}}</td>
                              <td class="text-center">{{ $bought->total_price}}</td>
                                <td class="text-center">{{ $bought->created_at }}</td>
                                <td class="text-center">{{ $bought->type }}</td>
                                <td class="text-center">{{ $bought->remain }}</td>

                                <td class="text-center">
                                    <a href="{{ route('admin.boughts.getUpdateBought', ['id' =>$bought->id])}}"
                                       class="btn btn-warning btn-sm">
                                        <li class="fa fa-pencil"> Edit</li>
                                    </a>

                                    <a class="btn btn-danger btn-sm" title="Delete" data-toggle="modal"
                                       href="#delete"
                                       data-id="{{$bought->id}}"
                                       data-token="{{csrf_token()}}">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </form>
        </div>
    </section>

@endsection

@section('modals')
    @include('admin.pages.boughts.modals.delete-bought')
@endsection
