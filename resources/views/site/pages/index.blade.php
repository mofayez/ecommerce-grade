@extends('site.layouts.master')

@section('page-content')

<div class="slider">
    <img src="{{asset('storage/images/slide3.jpg')}}" style="min-width: 100%;">
</div><!--End slider-->

<div class="page-content">
    <div class="container-fluid">
        <div class="main-widget">
            <div class="main-widget-title">
                <h3>Our Products</h3>
            </div><!--End main-widget-title-->
            <div class="main-widget-cont">
                <div  class="widgets_carousel5">

                    @foreach($products as $product)
                    <div class="product-item">
                        <div class="img-container">
                            <a href="product-item.html" class="product-img">
                                <img src="{{asset('storage/images/products/1.jpg')}}" alt="">
                            </a>
                            <div class="product-action">
                                <a href="">
                                    <i class="fa fa-shopping-cart"></i>
                                </a>
                                <a href="">
                                    <i class="fa fa-heart-o"></i>
                                </a>
                                <a href="">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <a href="#" data-toggle="modal" data-target="#productModal">
                                    <i class="fa fa-search-plus"></i>
                                </a>
                            </div><!--End product-action-->
                        </div><!--End img-container-->
                        <div class="product-desc">
                            <a href="product-item.html" class="product-name">
                                Product Name 1
                            </a><!--End product-name-->
                            <ul class="rating">
                                <li class="active"></li>
                                <li class="active"></li>
                                <li class="active"></li>
                                <li class="active"></li>
                                <li class="active"></li>
                            </ul><!--End rating-->
                            <div class="price-box">
                                <span class="price">$400.00</span>
                            </div><!--End price-box-->
                        </div><!--End product-desc-->
                    </div><!--End product-item-->
                    @endforeach


                </div><!--End widgets_carousel5-->
            </div><!--End main-widget-cont-->
        </div><!--End main-widget-->
        <div class="spacer-35"></div>
        <div class="main-widget">
            <div class="main-widget-title">
                <h3>Our Products</h3>
            </div><!--End main-widget-title-->
            <div class="main-widget-cont">
                <div class="tab-wrapper side-tab">
                    <ul class="tab">
                        <li>
                            <a href="#tab1">Mobiles &amp; Tablets</a>
                        </li>
                        <li>
                            <a href="#tab2">Electronics</a>
                        </li>
                        <li>
                            <a href="#tab3">Home Devices</a>
                        </li>
                        <li>
                            <a href="#tab4">Tourism &amp; travel</a>
                        </li>
                        <li>
                            <a href="#tab5">fashion</a>
                        </li>
                        <li>
                            <a href="#tab6">Health &amp; Beauty</a>
                        </li>
                        <li>
                            <a href="#tab7">watches</a>
                        </li>
                        <a href="products.html" class="view-all">
                            View All Products
                        </a>
                    </ul><!--End tab-->
                    <div class="tab-content">
                        <div id="tab1" class="tab-panel">
                            <div  class="widgets_carousel4">
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/1.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/2.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/3.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/4.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/5.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/6.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                            </div><!--End widgets_carousel4-->

                            <a class="custom-btn no-brdr-rd" href="">
                                Show More
                            </a>
                        </div><!--End description tab-panel-->
                        <div id="tab2" class="tab-panel">
                            <div  class="widgets_carousel4">
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/6.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/2.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/3.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/4.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/5.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/6.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                            </div><!--End widgets_carousel4-->
                            <a class="custom-btn no-brdr-rd" href="">
                                Show More
                            </a>
                        </div><!--End description tab-panel-->
                        <div id="tab3" class="tab-panel">
                            <div  class="widgets_carousel4">
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/8.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/2.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/3.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/4.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/5.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/6.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                            </div><!--End widgets_carousel4-->
                            <a class="custom-btn no-brdr-rd" href="">
                                Show More
                            </a>
                        </div><!--End description tab-panel-->
                        <div id="tab4" class="tab-panel">
                            <div  class="widgets_carousel4">
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/7.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/2.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/3.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/4.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/5.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/6.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                            </div><!--End widgets_carousel4-->
                            <a class="custom-btn no-brdr-rd" href="">
                                Show More
                            </a>
                        </div><!--End description tab-panel-->
                        <div id="tab5" class="tab-panel">
                            <div  class="widgets_carousel4">
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/8.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/2.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/3.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/4.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/5.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/6.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                            </div><!--End widgets_carousel4-->
                            <a class="custom-btn no-brdr-rd" href="">
                                Show More
                            </a>
                        </div><!--End description tab-panel-->
                        <div id="tab6" class="tab-panel">
                            <div  class="widgets_carousel4">
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/9.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/2.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/3.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/4.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/5.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/6.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                            </div><!--End widgets_carousel4-->
                            <a class="custom-btn no-brdr-rd" href="">
                                Show More
                            </a>
                        </div><!--End description tab-panel-->
                        <div id="tab7" class="tab-panel">
                            <div  class="widgets_carousel4">
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/8.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/2.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/3.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/4.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/5.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                                <div class="product-item">
                                    <div class="img-container">
                                        <a href="product-item.html" class="product-img">
                                            <img src="images/products/6.jpg" alt="">
                                        </a>
                                        <div class="product-action">
                                            <a href="">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div><!--End product-action-->
                                    </div><!--End img-container-->
                                    <div class="product-desc">
                                        <a href="product-item.html" class="product-name">
                                            Product Name
                                        </a><!--End product-name-->
                                        <ul class="rating">
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                        </ul><!--End rating-->
                                        <div class="price-box">
                                            <span class="price">$400.00</span>
                                        </div><!--End price-box-->
                                    </div><!--End product-desc-->
                                </div><!--End product-item-->
                            </div><!--End widgets_carousel4-->
                            <a class="custom-btn no-brdr-rd" href="">
                                Show More
                            </a>
                        </div><!--End description tab-panel-->
                    </div><!--End tab-content-->
                </div><!--End side-tab tab-wrapper-->
            </div><!--End main-widget-cont-->
        </div><!--End main-widget-->
        <div class="spacer-35"></div>
        <div class="row">
            <div class="col-md-9">
                <div class="main-widget">
                    <div class="main-widget-title">
                        <h3>
                            New Arrivals
                        </h3>
                    </div><!--End main-widget-title-->
                    <div class="main-widget-cont">
                        <div  class="widgets_carousel4">
                            <div class="product-item">
                                <div class="img-container">
                                    <a href="product-item.html" class="product-img">
                                        <img src="images/products/1.jpg" alt="">
                                    </a>
                                    <div class="product-action">
                                        <a href="">
                                            <i class="fa fa-shopping-cart"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-heart-o"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#productModal">
                                            <i class="fa fa-search-plus"></i>
                                        </a>
                                    </div><!--End product-action-->
                                </div><!--End img-container-->
                                <div class="product-desc">
                                    <a href="product-item.html" class="product-name">
                                        Product Name
                                    </a><!--End product-name-->
                                    <ul class="rating">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                    </ul><!--End rating-->
                                    <div class="price-box">
                                        <span class="price">$400.00</span>
                                    </div><!--End price-box-->
                                </div><!--End product-desc-->
                            </div><!--End product-item-->
                            <div class="product-item">
                                <div class="img-container">
                                    <a href="product-item.html" class="product-img">
                                        <img src="images/products/2.jpg" alt="">
                                    </a>
                                    <div class="product-action">
                                        <a href="">
                                            <i class="fa fa-shopping-cart"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-heart-o"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#productModal">
                                            <i class="fa fa-search-plus"></i>
                                        </a>
                                    </div><!--End product-action-->
                                </div><!--End img-container-->
                                <div class="product-desc">
                                    <a href="product-item.html" class="product-name">
                                        Product Name
                                    </a><!--End product-name-->
                                    <ul class="rating">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                    </ul><!--End rating-->
                                    <div class="price-box">
                                        <span class="price">$400.00</span>
                                    </div><!--End price-box-->
                                </div><!--End product-desc-->
                            </div><!--End product-item-->
                            <div class="product-item">
                                <div class="img-container">
                                    <a href="product-item.html" class="product-img">
                                        <img src="images/products/3.jpg" alt="">
                                    </a>
                                    <div class="product-action">
                                        <a href="">
                                            <i class="fa fa-shopping-cart"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-heart-o"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#productModal">
                                            <i class="fa fa-search-plus"></i>
                                        </a>
                                    </div><!--End product-action-->
                                </div><!--End img-container-->
                                <div class="product-desc">
                                    <a href="product-item.html" class="product-name">
                                        Product Name
                                    </a><!--End product-name-->
                                    <ul class="rating">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                    </ul><!--End rating-->
                                    <div class="price-box">
                                        <span class="price">$400.00</span>
                                    </div><!--End price-box-->
                                </div><!--End product-desc-->
                            </div><!--End product-item-->
                            <div class="product-item">
                                <div class="img-container">
                                    <a href="product-item.html" class="product-img">
                                        <img src="images/products/4.jpg" alt="">
                                    </a>
                                    <div class="product-action">
                                        <a href="">
                                            <i class="fa fa-shopping-cart"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-heart-o"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#productModal">
                                            <i class="fa fa-search-plus"></i>
                                        </a>
                                    </div><!--End product-action-->
                                </div><!--End img-container-->
                                <div class="product-desc">
                                    <a href="product-item.html" class="product-name">
                                        Product Name
                                    </a><!--End product-name-->
                                    <ul class="rating">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                    </ul><!--End rating-->
                                    <div class="price-box">
                                        <span class="price">$400.00</span>
                                    </div><!--End price-box-->
                                </div><!--End product-desc-->
                            </div><!--End product-item-->
                            <div class="product-item">
                                <div class="img-container">
                                    <a href="product-item.html" class="product-img">
                                        <img src="images/products/5.jpg" alt="">
                                    </a>
                                    <div class="product-action">
                                        <a href="">
                                            <i class="fa fa-shopping-cart"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-heart-o"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#productModal">
                                            <i class="fa fa-search-plus"></i>
                                        </a>
                                    </div><!--End product-action-->
                                </div><!--End img-container-->
                                <div class="product-desc">
                                    <a href="product-item.html" class="product-name">
                                        Product Name
                                    </a><!--End product-name-->
                                    <ul class="rating">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                    </ul><!--End rating-->
                                    <div class="price-box">
                                        <span class="price">$400.00</span>
                                    </div><!--End price-box-->
                                </div><!--End product-desc-->
                            </div><!--End product-item-->
                            <div class="product-item">
                                <div class="img-container">
                                    <a href="product-item.html" class="product-img">
                                        <img src="images/products/6.jpg" alt="">
                                    </a>
                                    <div class="product-action">
                                        <a href="">
                                            <i class="fa fa-shopping-cart"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-heart-o"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#productModal">
                                            <i class="fa fa-search-plus"></i>
                                        </a>
                                    </div><!--End product-action-->
                                </div><!--End img-container-->
                                <div class="product-desc">
                                    <a href="product-item.html" class="product-name">
                                        Product Name
                                    </a><!--End product-name-->
                                    <ul class="rating">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                    </ul><!--End rating-->
                                    <div class="price-box">
                                        <span class="price">$400.00</span>
                                    </div><!--End price-box-->
                                </div><!--End product-desc-->
                            </div><!--End product-item-->
                        </div><!--End widgets_carousel4-->
                    </div><!--End main-widget-cont-->
                </div><!--End main-widget-->
                <div class="spacer-35"></div>

            </div><!--End col-md-9-->
            <div class="col-md-3">
                <div class="side-widget">
                    <div class="side-widget-title">
                        <h3>best seller</h3>
                    </div><!--End side-widget-title-->
                    <div class="side-widget-cont widgets_carousel">
                        <div class="item">
                            <div class="product-item side-product">
                                <div class="img-container">
                                    <a href="product-item.html" class="product-img">
                                        <img src="images/products/1.jpg" alt="">
                                    </a>
                                </div><!--End img-container-->
                                <div class="product-desc">
                                    <a href="product-item.html" class="product-name">
                                        Product Name
                                    </a><!--End product-name-->
                                    <div class="price-box">
                                        <span class="price">$200.00</span>
                                        <s class="old-price">$300.00</s>
                                    </div><!--End price-box-->
                                    <ul class="rating">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                    </ul><!--End rating-->
                                </div><!--End product-desc-->
                            </div><!--End product-item-->
                            <div class="product-item side-product">
                                <div class="img-container">
                                    <a href="product-item.html" class="product-img">
                                        <img src="images/products/2.jpg" alt="">
                                    </a>
                                </div><!--End img-container-->
                                <div class="product-desc">
                                    <a href="product-item.html" class="product-name">
                                        Product Name
                                    </a><!--End product-name-->
                                    <div class="price-box">
                                        <span class="price">$200.00</span>
                                        <s class="old-price">$300.00</s>
                                    </div><!--End price-box-->
                                    <ul class="rating">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                    </ul><!--End rating-->
                                </div><!--End product-desc-->
                            </div><!--End product-item-->
                            <div class="product-item side-product">
                                <div class="img-container">
                                    <a href="product-item.html" class="product-img">
                                        <img src="images/products/3.jpg" alt="">
                                    </a>
                                </div><!--End img-container-->
                                <div class="product-desc">
                                    <a href="product-item.html" class="product-name">
                                        Product Name
                                    </a><!--End product-name-->
                                    <div class="price-box">
                                        <span class="price">$200.00</span>
                                        <s class="old-price">$300.00</s>
                                    </div><!--End price-box-->
                                    <ul class="rating">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                    </ul><!--End rating-->
                                </div><!--End product-desc-->
                            </div><!--End product-item-->
                            <div class="product-item side-product">
                                <div class="img-container">
                                    <a href="product-item.html" class="product-img">
                                        <img src="images/products/4.jpg" alt="">
                                    </a>
                                </div><!--End img-container-->
                                <div class="product-desc">
                                    <a href="product-item.html" class="product-name">
                                        Product Name
                                    </a><!--End product-name-->
                                    <div class="price-box">
                                        <span class="price">$200.00</span>
                                        <s class="old-price">$300.00</s>
                                    </div><!--End price-box-->
                                    <ul class="rating">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                    </ul><!--End rating-->
                                </div><!--End product-desc-->
                            </div><!--End product-item-->
                        </div><!--End item-->
                        <div class="item">
                            <div class="product-item side-product">
                                <div class="img-container">
                                    <a href="product-item.html" class="product-img">
                                        <img src="images/products/1.jpg" alt="">
                                    </a>
                                </div><!--End img-container-->
                                <div class="product-desc">
                                    <a href="product-item.html" class="product-name">
                                        Product Name
                                    </a><!--End product-name-->
                                    <div class="price-box">
                                        <span class="price">$200.00</span>
                                        <s class="old-price">$300.00</s>
                                    </div><!--End price-box-->
                                    <ul class="rating">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                    </ul><!--End rating-->
                                </div><!--End product-desc-->
                            </div><!--End product-item-->
                            <div class="product-item side-product">
                                <div class="img-container">
                                    <a href="product-item.html" class="product-img">
                                        <img src="images/products/2.jpg" alt="">
                                    </a>
                                </div><!--End img-container-->
                                <div class="product-desc">
                                    <a href="product-item.html" class="product-name">
                                        Product Name
                                    </a><!--End product-name-->
                                    <div class="price-box">
                                        <span class="price">$200.00</span>
                                        <s class="old-price">$300.00</s>
                                    </div><!--End price-box-->
                                    <ul class="rating">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                    </ul><!--End rating-->
                                </div><!--End product-desc-->
                            </div><!--End product-item-->
                            <div class="product-item side-product">
                                <div class="img-container">
                                    <a href="product-item.html" class="product-img">
                                        <img src="images/products/3.jpg" alt="">
                                    </a>
                                </div><!--End img-container-->
                                <div class="product-desc">
                                    <a href="product-item.html" class="product-name">
                                        Product Name
                                    </a><!--End product-name-->
                                    <div class="price-box">
                                        <span class="price">$200.00</span>
                                        <s class="old-price">$300.00</s>
                                    </div><!--End price-box-->
                                    <ul class="rating">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                    </ul><!--End rating-->
                                </div><!--End product-desc-->
                            </div><!--End product-item-->
                            <div class="product-item side-product">
                                <div class="img-container">
                                    <a href="product-item.html" class="product-img">
                                        <img src="images/products/4.jpg" alt="">
                                    </a>
                                </div><!--End img-container-->
                                <div class="product-desc">
                                    <a href="product-item.html" class="product-name">
                                        Product Name
                                    </a><!--End product-name-->
                                    <div class="price-box">
                                        <span class="price">$200.00</span>
                                        <s class="old-price">$300.00</s>
                                    </div><!--End price-box-->
                                    <ul class="rating">
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                    </ul><!--End rating-->
                                </div><!--End product-desc-->
                            </div><!--End product-item-->
                        </div><!--End item-->
                    </div><!--End side-widget-cont-->
                </div><!--End side-widget-->
            </div><!--End col-md-3-->
        </div><!--End row-->
    </div><!--End container-fluid-->
</div><!--End page-content-->

<!-- product modal -->
<div id="productModal" class="modal fade">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">×</span></button>
                    </div><!--End modal-header-->
                    <div class="modal-body">
                        <div class="pro-modal-wrap">
                            <div class="pro-modal-img">
                                <img id="show-product"
                                src="images/products/4.jpg"
                                data-zoom-image="images/products/4.jpg">
                            </div><!--general-img-->
                            <div class="pro-modal-cont">
                                <h3>
                                    LG TV
                                </h3>
                                <div class="price-box">
                                    <span class="price">$ 400</span>
                                    <s class="old-price">$ 420</s>
                                </div><!--End price-box-->
                                <p>

                                    Cras varius. Ut leo. Vivamus in erat ut urna cursus vestibulum. Aliquam erat volutpat.

                                    Proin magna. Nunc interdum lacus sit amet orci. Fusce vel dui.
                                </p>
                                <div class="product-item-cart">
                                    <form onsubmit="return false;" class="cart-form">
                                    <span>quantity</span>
                                    <div class="cat-number">
                                        <a href="#" class="number-down-modal">
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                        <input value="1" max="21" class="form-control"
                                        name="quantity" type="text">
                                        <a href="#" class="number-up-modal">
                                            <i class="fa fa-angle-up"></i>
                                        </a>
                                    </div>
                                    <div class="row">
                                        <button class="cart-btn text-icon-btn" data-quantity="1"
                                        style="margin-top: 5%">
                                        <i class="fa fa-shopping-cart"></i>
                                        add to cart
                                    </button>
                                    <button type="button" class="icon-btn wishlist-btn"
                                    data-url="#">
                                    <i class="fa fa-heart"></i>
                                </button>
                            </div>
                        </form>
                    </div><!--End product-item-cart-->
                    <div class="product-item-share">
                        <span>share</span>
                        <ul class="social-share">
                            <li>
                                <a href="https://www.google+.com/sharer/sharer.php?u=productLink&display=popu">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.twitter.com/sharer/sharer.php?u=productLink&display=popu">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/sharer/sharer.php?u=productLink&display=popu">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                        </ul><!--End social-share-->
                    </div><!--End product-item-share-->
                </div><!--End pro-modal-cont-->
            </div><!--End pro-modal-wrap-->
        </div><!--End modal-body-->
    </div><!--End modal-content -->


@endsection
