<header class="header">
    <a class="col-xs-5 col-sm-3 col-md-2 logo" href="{{route('site.home.index')}}">
        <img src="{{asset('storage/images/logo.png')}}">
    </a><!--End logo-->
    <div class="col-xs-5 col-sm-7 col-md-9 main-header">
        <div class="header-top">
            <div class="navbar top-navbar" role="navigation">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".top-navbar .navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <nav class="collapse collapsing navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="#">Arabic</a></li>
                        <li><a href="{{route('site.getLogin')}}">Sign in/up</a></li>
                        <li><a href="">Checkout</a></li>
                        <li><a href="profile.html">My Account</a></li>
                        <li><a href="faq.html">FAQs</a></li>
                    </ul><!--End navbar-nav-->
                </nav><!--End navbar-collapse-->
            </div><!--End top-navbar-->
            <form class="form-icon head-search">
                <input type="text" placeholder="Enter Key Search">
                <button type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </form><!--End head-search-->
            <a class="head-wish" href="">
                <i class="fa fa-heart"></i>
                <span>3</span>
            </a>
        </div><!--End header-top-->
        <div class="menu-toggle">
            <span>menu toggle</span>
        </div>
        <div class="site-menu">
            <div class="menu-toggle">
                menu toggle
            </div>
            <ul class="menu-nav">

                @foreach($menus as $menu)

                    <li>
                        <a href="#">{{$menu->trans->menu}}</a>
                        <div class="main-drop full-mega">
                            @foreach($menu->categories as $main_category)
                                <div class="col-md-3">

                                    <h3>{{$main_category->trans->category}}</h3>

                                    @foreach($main_category->sub_categories as $sub_category)

                                        <a href="#">{{$sub_category->trans->category}}</a>

                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </li>
                @endforeach
            </ul><!--End menu-nav -->
        </div><!--End site-menu-->
    </div><!--End main-header-->

    <div class="col-xs-2 col-sm-2 col-md-1 shop-cart">
        <a class="cart-title" href="">
            <i class="fa fa-shopping-cart"></i>
            <span class="cart-text">my cart</span>
            <span class="cart-count">1</span>
        </a><!--End cart-title-->
        <div class="cart-cont">
            <div class="cart-items">
                <a href="product-item.html">
                    <img src="images/products/1.jpg">
                    <p class="product-name">
                        Men Shirt
                    </p>
                    <div class="product-price">
                        1 x
                        <span class="price">$5.00</span>
                    </div>
                    <div class="product-remove">
                        <i class="fa fa-close"></i>
                    </div><!-- .product-remove -->
                </a>
            </div><!--End cart-items-->
            <div class="shipping">
                <p>Total</p>
                <p>$50.00</p>
            </div>
            <a href="" class="cart-button">Checkout</a>
        </div><!--End cart-cont-->
    </div><!--End shop-cart-->

</header><!--End header-->
