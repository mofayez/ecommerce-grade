@extends('site.layouts.master')

@section('page-content')

<ul class="breadcrumb">
          <li>
            <a href="{{route('site.home.index')}}">
              <i class="fa fa-home"></i> Home
            </a>
          </li>
          <li class="active">Sign in &amp; up</li>
        </ul><!--End breadcrumb-->

        <div class="page-content">
          <div class="container">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                <div class="box-wrap brdr-rd-3">
                  <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                      <div class="login-register">
                        <ul class="nav nav-tabs">
                          <li class="active">
                            <a href="#login" data-toggle="tab">
                              Login
                            </a>
                          </li>
                          <li>
                            <a href="#register" data-toggle="tab">
                              Register
                            </a>
                          </li>
                        </ul><!--End nav-tabs-->
                        <div class="tab-content">

                          <div class="tab-pane fade in active" id="login">
                            <form class="dialog-form" id="head-log-form"
                                action="{{route('site.postLogin')}}" method="POST">

                                {{csrf_field()}}

                              <div id="headLogActionSuccess" class="alert alert-success hidden"></div>
                              <div id="headLogActionError" class="alert alert-danger hidden"></div>
                              <div class="form-group">
                                <input id="login-email" class="form-control" placeholder="email"
                                type="email" name="email">
                                @if($errors->has('email'))
                                    <small class="has-error" style="color: red;">
                                        {{$errors->first('email')}}
                                    </small>
                                @endif
                              </div><!--End col-md-6-->
                              <div class="form-group">
                                <input id="login-pass" class="form-control" placeholder="password"
                                type="password" name="password">
                                @if($errors->has('password'))
                                    <small class="has-error" style="color: red;">
                                        {{$errors->first('password')}}
                                    </small>
                                @endif
                              </div><!--End form-group-->
                              <div class="form-group">
                                <div class="remmeber">
                                  <input id="remmeber" type="checkbox" name="remember">
                                  <label for="remmeber">
                                    remember
                                  </label>
                                </div>
                                <div class="forget-pass">
                                  <a href="#">
                                    Forget Password
                                  </a>
                                </div>
                              </div>
                              <button type="submit" class="custom-btn">Login</button>
                              <div class="text-spacer">
                                <p>Login with</p>
                              </div>
                              <ul class="scoial-login">
                                <li>
                                  <a href="#">
                                    <i class="fa fa-facebook"></i>
                                  </a>
                                </li>
                                <li>

                                  <a href="#">
                                    <i class="fa fa-twitter"></i>
                                  </a>
                                </li>
                                <li>
                                  <a href="#">
                                    <i class="fa fa-google-plus"></i>
                                  </a>
                                </li>
                              </ul>
                            </form>
                          </div><!--End tab-pane-->
                          <div class="tab-pane fade in" id="register">
                            <form class="dialog-form" id="reg-form"
                                action="{{route('site.postRegister')}}"
                                method="POST">

                                {{csrf_field()}}

                              <div class="alert alert-success hidden"
                              id="reg-alert-success">
                              <strong>Success!</strong> You registered successfully!
                            </div>
                            <div class="alert alert-danger hidden"
                            id="reg-alert-error">
                            <strong>Error!</strong> You are not registered successfully.
                          </div>

                          <div class="form-group">
                            <input type="text" class="form-control"
                            name="name"
                            placeholder="Name" data-msg-required="name is required">
                            @if($errors->has('name'))
                                <small class="has-error" style="color: red;">
                                    {{$errors->first('name')}}
                                </small>
                            @endif
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control"
                            name="phone"
                            placeholder="phone"  data-msg-required="phone is required">
                            @if($errors->has('phone'))
                                <small class="has-error" style="color: red;">
                                    {{$errors->first('phone')}}
                                </small>
                            @endif
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control"
                            placeholder="address"
                            name="address" data-msg-required="address name is required">
                            @if($errors->has('address'))
                            <small class="has-error" style="color: red;">
                                {{$errors->first('address')}}
                            </small>
                            @endif
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control"
                            placeholder="Postal code"
                            name="postal" data-msg-required="postal code name is required">
                            @if($errors->has('postal'))
                            <small class="has-error" style="color: red;">
                                {{$errors->first('postal')}}
                            </small>
                            @endif
                          </div>
                          <div class="form-group">
                            <select type="text" class="form-control"
                                name="gender" data-msg-required="gender name is required">
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <input type="email" placeholder="email"
                                class="form-control" name="email"
                                data-msg-required="name is required">
                                @if($errors->has('email'))
                                    <small class="has-error" style="color: red;">
                                        {{$errors->first('email')}}
                                    </small>
                                @endif
                          </div>
                          <div class="form-group">
                            <input type="password" id="reg_pass"
                            placeholder="password"
                            class="form-control"
                            name="password" data-msg-required="passwordname is required">
                            @if($errors->has('password'))
                                <small class="has-error" style="color: red;">
                                    {{$errors->first('password')}}
                                </small>
                            @endif
                          </div>
                          <div class="form-group">
                            <input type="password" id="reg_password_confirmation"
                            placeholder="confirm password"
                            class="form-control" name="password_confirmation"  data-msg-required="name is required">
                          </div>
                          <button type="submit" class="custom-btn"
                          >register</button>

                        </form>
                      </div>
                    </div><!--End tab-content-->
                  </div><!--End login-register-->
                </div><!--End col-md-10-->
              </div><!--End row-->
            </div><!--End box-wrap-->
          </div><!--End col-md-8-->
        </div><!--End Row-->

@endsection
